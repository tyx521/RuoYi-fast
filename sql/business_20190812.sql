DROP TABLE IF EXISTS `st_bond`;
CREATE TABLE `st_bond`  (
  `bond_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '债券id',
  `bond_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '代码',
  `bond_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '名称',
  `bond_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '类型（1：地方债 2：国债 3：回购 4：可转债 5：企业债 6：贴债）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`bond_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '债券表' ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `st_bond_detail`;
CREATE TABLE `st_bond_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `bond_id` bigint(20) NOT NULL COMMENT '债券id',
  `data_time` date NOT NULL COMMENT '日期',
  `interest` decimal(20, 3) DEFAULT NULL COMMENT '利率',
  `rates` decimal(20, 3) DEFAULT NULL COMMENT '利息',
  `up_percent` decimal(20, 3) DEFAULT NULL COMMENT '涨幅%',
  `now_price` decimal(20, 3) DEFAULT NULL COMMENT '现价',
  `total_hands` decimal(20, 0) DEFAULT NULL COMMENT '总手',
  `prev_close` decimal(20, 3) DEFAULT NULL COMMENT '昨收',
  `open_price` decimal(20, 3) DEFAULT NULL COMMENT '开盘',
  `highest_price` decimal(20, 3) DEFAULT NULL COMMENT '最高价',
  `lowest_price` decimal(20, 3) DEFAULT NULL COMMENT '最低价',
  `buy_price` decimal(20, 3) DEFAULT NULL COMMENT '买价',
  `sell_price` decimal(20, 3) DEFAULT NULL COMMENT '卖价',
  `total_amount` decimal(20, 0) DEFAULT NULL COMMENT '总金额',
  `buy_volume` decimal(20, 0) DEFAULT NULL COMMENT '买量',
  `sell_volume` decimal(20, 0) DEFAULT NULL COMMENT '卖量',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '债券详情表' ROW_FORMAT = Dynamic;





DROP TABLE IF EXISTS `st_foundation`;
CREATE TABLE `st_foundation`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `found_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '代码',
  `found_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '名称',
  `bond_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '类型（1：股票型 2：混合型 3：债券型 4：货币型 5：短期理财 6：QDII 7：保本型  8：指数型）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '基金表' ROW_FORMAT = Dynamic;




DROP TABLE IF EXISTS `st_foundation_detail`;
CREATE TABLE `st_foundation_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `found_id` bigint(20) NOT NULL COMMENT '基金id',
  `data_time` date NOT NULL COMMENT '日期',
  `net_value` decimal(20, 3) DEFAULT NULL COMMENT '净值',
  `total_net_value` decimal(20, 3) DEFAULT NULL COMMENT '累计净值',
  `day_percent` decimal(20, 3) DEFAULT NULL COMMENT '日增长率',
  `week_percent` decimal(20, 3) DEFAULT NULL COMMENT '近一周',
  `month_percent` decimal(20, 3) DEFAULT NULL COMMENT '近一月',
  `three_month_percent` decimal(20, 3) DEFAULT NULL COMMENT '近三月',
  `year_percent` decimal(20, 3) DEFAULT NULL COMMENT '近一年',
  `establish_percent` decimal(20, 3) DEFAULT NULL COMMENT '成立以来',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '基金详情表' ROW_FORMAT = Dynamic;


DROP TABLE IF EXISTS `st_foreign_exchangen`;
CREATE TABLE `st_foreign_exchangen`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `exchangen_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '代码',
  `exchangen_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '名称',
  `exchangen_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '类型（1：基本汇率 2：反向汇率 3：交叉汇率）',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '外汇表' ROW_FORMAT = Dynamic;



DROP TABLE IF EXISTS `st_foreign_exchangen_detail`;
CREATE TABLE `st_foreign_exchangen_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `exchangen_id` bigint(20) NOT NULL COMMENT '外汇id',
  `data_time` date NOT NULL COMMENT '日期',
  `now_price` decimal(10, 5) DEFAULT NULL COMMENT '现价',
  `up_down_val` decimal(10, 5) DEFAULT NULL COMMENT '涨跌',
  `buy_price` decimal(10, 5) DEFAULT NULL COMMENT '买价',
  `sell_price` decimal(10, 5) DEFAULT NULL COMMENT '卖价',
  `amplitude` decimal(10, 2) DEFAULT NULL COMMENT '振幅',
  `up_percent` decimal(20, 2) DEFAULT NULL COMMENT '涨幅%',
  `up_percent_one` decimal(20, 2) DEFAULT NULL COMMENT '1分钟涨幅%',
  `up_percent_three` decimal(20, 2) DEFAULT NULL COMMENT '3分钟涨幅%',
  `highest_price` decimal(10, 5) DEFAULT NULL COMMENT '最高价',
  `lowest_price` decimal(10, 5) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(10, 5) DEFAULT NULL COMMENT '开盘',
  `runout` decimal(20, 0) DEFAULT NULL COMMENT '跳动度',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 68 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '外汇详情表' ROW_FORMAT = Dynamic;



DROP TABLE IF EXISTS `st_futures_category`;
CREATE TABLE `st_futures_category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `node_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '期货分类名称',
  `code` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '编码',
  `parent_id` int(11) NOT NULL COMMENT '父ID',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '期货分类表' ROW_FORMAT = Dynamic;



DROP TABLE IF EXISTS `st_futures`;
CREATE TABLE `st_futures`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '期货id',
  `future_code` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '代码',
  `future_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '名称',
  `future_type` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '类型',
  `del_flag` char(1) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '期货表' ROW_FORMAT = Dynamic;



DROP TABLE IF EXISTS `st_futures_detail`;
CREATE TABLE `st_futures_detail`  (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `future_id` int(20) NOT NULL COMMENT '期货id',
  `data_time` date NOT NULL COMMENT '日期',
  `now_price` decimal(20, 3) DEFAULT NULL COMMENT '现价',
  `buy_price` decimal(20, 3) DEFAULT NULL COMMENT '买价',
  `sell_price` decimal(20, 3) NOT NULL COMMENT '卖价',
  `buy_volume` decimal(20, 0) DEFAULT NULL COMMENT '买量',
  `sell_volume` decimal(20, 0) DEFAULT NULL COMMENT '卖量',
  `highest_price` decimal(20, 3) DEFAULT NULL COMMENT '最高价',
  `lowest_price` decimal(20, 3) DEFAULT NULL COMMENT '最低价',
  `open_price` decimal(20, 3) DEFAULT NULL COMMENT '开盘',
  `prev_close` decimal(20, 3) DEFAULT NULL COMMENT '昨收',
  `prev_settle` decimal(20, 3) DEFAULT NULL COMMENT '昨结',
  `today_settle` decimal(20, 3) DEFAULT NULL COMMENT '今结',
  `up_down_val` decimal(10, 5) DEFAULT NULL COMMENT '涨跌',
  `up_percent` decimal(20, 3) DEFAULT NULL COMMENT '涨幅%',
  `now_hands` decimal(20, 0) DEFAULT NULL COMMENT '现手',
  `total_hands` decimal(20, 0) DEFAULT NULL COMMENT '总手',
  `hold_position` decimal(20, 0) DEFAULT NULL COMMENT '持仓',
  `buy_amount` decimal(20, 0) DEFAULT NULL COMMENT '外盘',
  `sell_amount` decimal(20, 0) DEFAULT NULL COMMENT '内盘',
  `amplitude` decimal(10, 2) DEFAULT NULL COMMENT '振幅',
  `positions` decimal(20, 0) DEFAULT NULL COMMENT '日增仓',
  `total_amount` decimal(20, 0) DEFAULT NULL COMMENT '总金额',
  `create_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '期货详情表' ROW_FORMAT = Dynamic;

