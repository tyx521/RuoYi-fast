-- ----------------------------
-- 1、债券表创建索引
-- ----------------------------
ALTER TABLE st_bond ADD UNIQUE (bond_code);                   --创建唯一索引
ALTER TABLE st_bond_detail ADD UNIQUE (bond_id,data_time);    --创建唯一索引

-- ----------------------------
-- 2、基金表创建索引
-- ----------------------------
ALTER TABLE st_foundation ADD UNIQUE (found_code);                   --创建唯一索引
ALTER TABLE st_foundation_detail ADD UNIQUE (found_id,data_time);    --创建唯一索引