package com.ruoyi.project.stock.exchangen.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 外汇详情对象 st_foreign_exchangen_detail
 * 
 * @author ruoyi
 * @date 2019-08-19
 */
public class StForeignExchangenDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 外汇id */
    @Excel(name = "外汇id")
    private Long exchangenId;

    /** 日期 */
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dataTime;

    /** 现价 */
    @Excel(name = "现价")
    private Double nowPrice;

    /** 涨跌 */
    @Excel(name = "涨跌")
    private Double upDownVal;

    /** 买价 */
    @Excel(name = "买价")
    private Double buyPrice;

    /** 卖价 */
    @Excel(name = "卖价")
    private Double sellPrice;

    /** 振幅 */
    @Excel(name = "振幅")
    private Double amplitude;

    /** 涨幅% */
    @Excel(name = "涨幅%")
    private Double upPercent;

    /** 1分钟涨幅% */
    @Excel(name = "1分钟涨幅%")
    private Double upPercentOne;

    /** 3分钟涨幅% */
    @Excel(name = "3分钟涨幅%")
    private Double upPercentThree;

    /** 最高价 */
    @Excel(name = "最高价")
    private Double highestPrice;

    /** 最低价 */
    @Excel(name = "最低价")
    private Double lowestPrice;

    /** 开盘 */
    @Excel(name = "开盘")
    private Double openPrice;

    /** 跳动度 */
    @Excel(name = "跳动度")
    private Long runout;

    private StForeignExchangen exchangen;

    public StForeignExchangen getExchangen() {
        return exchangen;
    }

    public void setExchangen(StForeignExchangen exchangen) {
        this.exchangen = exchangen;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setExchangenId(Long exchangenId) 
    {
        this.exchangenId = exchangenId;
    }

    public Long getExchangenId() 
    {
        return exchangenId;
    }
    public void setDataTime(Date dataTime) 
    {
        this.dataTime = dataTime;
    }

    public Date getDataTime() 
    {
        return dataTime;
    }
    public void setNowPrice(Double nowPrice) 
    {
        this.nowPrice = nowPrice;
    }

    public Double getNowPrice() 
    {
        return nowPrice;
    }
    public void setUpDownVal(Double upDownVal) 
    {
        this.upDownVal = upDownVal;
    }

    public Double getUpDownVal() 
    {
        return upDownVal;
    }
    public void setBuyPrice(Double buyPrice) 
    {
        this.buyPrice = buyPrice;
    }

    public Double getBuyPrice() 
    {
        return buyPrice;
    }
    public void setSellPrice(Double sellPrice) 
    {
        this.sellPrice = sellPrice;
    }

    public Double getSellPrice() 
    {
        return sellPrice;
    }
    public void setAmplitude(Double amplitude) 
    {
        this.amplitude = amplitude;
    }

    public Double getAmplitude() 
    {
        return amplitude;
    }
    public void setUpPercent(Double upPercent) 
    {
        this.upPercent = upPercent;
    }

    public Double getUpPercent() 
    {
        return upPercent;
    }
    public void setUpPercentOne(Double upPercentOne) 
    {
        this.upPercentOne = upPercentOne;
    }

    public Double getUpPercentOne() 
    {
        return upPercentOne;
    }
    public void setUpPercentThree(Double upPercentThree) 
    {
        this.upPercentThree = upPercentThree;
    }

    public Double getUpPercentThree() 
    {
        return upPercentThree;
    }
    public void setHighestPrice(Double highestPrice) 
    {
        this.highestPrice = highestPrice;
    }

    public Double getHighestPrice() 
    {
        return highestPrice;
    }
    public void setLowestPrice(Double lowestPrice) 
    {
        this.lowestPrice = lowestPrice;
    }

    public Double getLowestPrice() 
    {
        return lowestPrice;
    }
    public void setOpenPrice(Double openPrice) 
    {
        this.openPrice = openPrice;
    }

    public Double getOpenPrice() 
    {
        return openPrice;
    }
    public void setRunout(Long runout) 
    {
        this.runout = runout;
    }

    public Long getRunout() 
    {
        return runout;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("exchangenId", getExchangenId())
            .append("dataTime", getDataTime())
            .append("nowPrice", getNowPrice())
            .append("upDownVal", getUpDownVal())
            .append("buyPrice", getBuyPrice())
            .append("sellPrice", getSellPrice())
            .append("amplitude", getAmplitude())
            .append("upPercent", getUpPercent())
            .append("upPercentOne", getUpPercentOne())
            .append("upPercentThree", getUpPercentThree())
            .append("highestPrice", getHighestPrice())
            .append("lowestPrice", getLowestPrice())
            .append("openPrice", getOpenPrice())
            .append("runout", getRunout())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
