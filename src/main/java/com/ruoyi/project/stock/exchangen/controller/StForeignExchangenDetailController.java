package com.ruoyi.project.stock.exchangen.controller;

import java.util.List;
import com.ruoyi.project.stock.exchangen.domain.StForeignExchangenDetail;
import com.ruoyi.project.stock.exchangen.service.IStForeignExchangenDetailService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 外汇详情Controller
 * 
 * @author ruoyi
 * @date 2019-08-19
 */
@Controller
@RequestMapping("/stock/exchangen/detail")
public class StForeignExchangenDetailController extends BaseController
{
    private String prefix = "stock/exchangen/detail";

    @Autowired
    private IStForeignExchangenDetailService stForeignExchangenDetailService;

    @RequiresPermissions("stock:exchangen:detail:view")
    @GetMapping()
    public String detail()
    {
        return prefix + "/detail";
    }

    /**
     * 查询外汇详情列表
     */
    @RequiresPermissions("stock:exchangen:detail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StForeignExchangenDetail stForeignExchangenDetail)
    {
        startPage();
        //List<StForeignExchangenDetail> list = stForeignExchangenDetailService.selectStForeignExchangenDetailList(stForeignExchangenDetail);
        List<StForeignExchangenDetail> list = stForeignExchangenDetailService.selectStForeignExchangenAndDetailAllInfo(stForeignExchangenDetail);
        return getDataTable(list);
    }

    /**
     * 导出外汇详情列表
     */
    @RequiresPermissions("stock:exchangen:detail:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StForeignExchangenDetail stForeignExchangenDetail)
    {
        List<StForeignExchangenDetail> list = stForeignExchangenDetailService.selectStForeignExchangenDetailList(stForeignExchangenDetail);
        ExcelUtil<StForeignExchangenDetail> util = new ExcelUtil<StForeignExchangenDetail>(StForeignExchangenDetail.class);
        return util.exportExcel(list, "detail");
    }

    /**
     * 新增外汇详情
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存外汇详情
     */
    @RequiresPermissions("stock:exchangen:detail:add")
    @Log(title = "外汇详情", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StForeignExchangenDetail stForeignExchangenDetail)
    {
        return toAjax(stForeignExchangenDetailService.insertStForeignExchangenDetail(stForeignExchangenDetail));
    }

    /**
     * 修改外汇详情
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StForeignExchangenDetail stForeignExchangenDetail = stForeignExchangenDetailService.selectStForeignExchangenDetailById(id);
        mmap.put("stForeignExchangenDetail", stForeignExchangenDetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存外汇详情
     */
    @RequiresPermissions("stock:exchangen:detail:edit")
    @Log(title = "外汇详情", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StForeignExchangenDetail stForeignExchangenDetail)
    {
        return toAjax(stForeignExchangenDetailService.updateStForeignExchangenDetail(stForeignExchangenDetail));
    }

    /**
     * 删除外汇详情
     */
    @RequiresPermissions("stock:exchangen:detail:remove")
    @Log(title = "外汇详情", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stForeignExchangenDetailService.deleteStForeignExchangenDetailByIds(ids));
    }
}
