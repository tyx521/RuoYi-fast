package com.ruoyi.project.stock.exchangen.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.stock.exchangen.mapper.StForeignExchangenMapper;
import com.ruoyi.project.stock.exchangen.domain.StForeignExchangen;
import com.ruoyi.project.stock.exchangen.service.IStForeignExchangenService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 外汇Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-19
 */
@Service
public class StForeignExchangenServiceImpl implements IStForeignExchangenService 
{
    @Autowired
    private StForeignExchangenMapper stForeignExchangenMapper;

    /**
     * 查询外汇
     * 
     * @param id 外汇ID
     * @return 外汇
     */
    @Override
    public StForeignExchangen selectStForeignExchangenById(Long id)
    {
        return stForeignExchangenMapper.selectStForeignExchangenById(id);
    }

    /**
     * 查询外汇列表
     * 
     * @param stForeignExchangen 外汇
     * @return 外汇
     */
    @Override
    public List<StForeignExchangen> selectStForeignExchangenList(StForeignExchangen stForeignExchangen)
    {
        return stForeignExchangenMapper.selectStForeignExchangenList(stForeignExchangen);
    }

    /**
     * 新增外汇
     * 
     * @param stForeignExchangen 外汇
     * @return 结果
     */
    @Override
    public int insertStForeignExchangen(StForeignExchangen stForeignExchangen)
    {
        stForeignExchangen.setCreateTime(DateUtils.getNowDate());
        return stForeignExchangenMapper.insertStForeignExchangen(stForeignExchangen);
    }

    /**
     * 修改外汇
     * 
     * @param stForeignExchangen 外汇
     * @return 结果
     */
    @Override
    public int updateStForeignExchangen(StForeignExchangen stForeignExchangen)
    {
        stForeignExchangen.setUpdateTime(DateUtils.getNowDate());
        return stForeignExchangenMapper.updateStForeignExchangen(stForeignExchangen);
    }

    /**
     * 删除外汇对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStForeignExchangenByIds(String ids)
    {
        return stForeignExchangenMapper.deleteStForeignExchangenByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除外汇信息
     * 
     * @param id 外汇ID
     * @return 结果
     */
    public int deleteStForeignExchangenById(Long id)
    {
        return stForeignExchangenMapper.deleteStForeignExchangenById(id);
    }

    /**
     * 查询外汇
     *
     * @param code 外汇代码
     * @return 外汇
     */
    public StForeignExchangen selectStForeignExchangenByCode(String code){
        return stForeignExchangenMapper.selectStForeignExchangenByCode(code);
    }
}
