package com.ruoyi.project.stock.exchangen.mapper;

import com.ruoyi.project.stock.exchangen.domain.StForeignExchangen;
import java.util.List;

/**
 * 外汇Mapper接口
 * 
 * @author ruoyi
 * @date 2019-08-19
 */
public interface StForeignExchangenMapper 
{
    /**
     * 查询外汇
     * 
     * @param id 外汇ID
     * @return 外汇
     */
    public StForeignExchangen selectStForeignExchangenById(Long id);

    /**
     * 查询外汇列表
     * 
     * @param stForeignExchangen 外汇
     * @return 外汇集合
     */
    public List<StForeignExchangen> selectStForeignExchangenList(StForeignExchangen stForeignExchangen);

    /**
     * 新增外汇
     * 
     * @param stForeignExchangen 外汇
     * @return 结果
     */
    public int insertStForeignExchangen(StForeignExchangen stForeignExchangen);

    /**
     * 修改外汇
     * 
     * @param stForeignExchangen 外汇
     * @return 结果
     */
    public int updateStForeignExchangen(StForeignExchangen stForeignExchangen);

    /**
     * 删除外汇
     * 
     * @param id 外汇ID
     * @return 结果
     */
    public int deleteStForeignExchangenById(Long id);

    /**
     * 批量删除外汇
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStForeignExchangenByIds(String[] ids);




    /**
     * 查询外汇
     *
     * @param id 外汇ID
     * @return 外汇
     */
    public StForeignExchangen selectStForeignExchangenByCode(String code);
}
