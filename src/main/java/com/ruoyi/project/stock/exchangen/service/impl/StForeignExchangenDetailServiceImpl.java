package com.ruoyi.project.stock.exchangen.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.stock.exchangen.domain.StForeignExchangenDetail;
import com.ruoyi.project.stock.exchangen.mapper.StForeignExchangenDetailMapper;
import com.ruoyi.project.stock.exchangen.service.IStForeignExchangenDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 外汇详情Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-19
 */
@Service
public class StForeignExchangenDetailServiceImpl implements IStForeignExchangenDetailService
{
    @Autowired
    private StForeignExchangenDetailMapper stForeignExchangenDetailMapper;

    /**
     * 查询外汇详情
     * 
     * @param id 外汇详情ID
     * @return 外汇详情
     */
    @Override
    public StForeignExchangenDetail selectStForeignExchangenDetailById(Long id)
    {
        return stForeignExchangenDetailMapper.selectStForeignExchangenDetailById(id);
    }

    /**
     * 查询外汇详情列表
     * 
     * @param stForeignExchangenDetail 外汇详情
     * @return 外汇详情
     */
    @Override
    public List<StForeignExchangenDetail> selectStForeignExchangenDetailList(StForeignExchangenDetail stForeignExchangenDetail)
    {
        return stForeignExchangenDetailMapper.selectStForeignExchangenDetailList(stForeignExchangenDetail);
    }

    /**
     * 新增外汇详情
     * 
     * @param stForeignExchangenDetail 外汇详情
     * @return 结果
     */
    @Override
    public int insertStForeignExchangenDetail(StForeignExchangenDetail stForeignExchangenDetail)
    {
        stForeignExchangenDetail.setCreateTime(DateUtils.getNowDate());
        return stForeignExchangenDetailMapper.insertStForeignExchangenDetail(stForeignExchangenDetail);
    }

    /**
     * 修改外汇详情
     * 
     * @param stForeignExchangenDetail 外汇详情
     * @return 结果
     */
    @Override
    public int updateStForeignExchangenDetail(StForeignExchangenDetail stForeignExchangenDetail)
    {
        stForeignExchangenDetail.setUpdateTime(DateUtils.getNowDate());
        return stForeignExchangenDetailMapper.updateStForeignExchangenDetail(stForeignExchangenDetail);
    }

    /**
     * 删除外汇详情对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStForeignExchangenDetailByIds(String ids)
    {
        return stForeignExchangenDetailMapper.deleteStForeignExchangenDetailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除外汇详情信息
     * 
     * @param id 外汇详情ID
     * @return 结果
     */
    public int deleteStForeignExchangenDetailById(Long id)
    {
        return stForeignExchangenDetailMapper.deleteStForeignExchangenDetailById(id);
    }

    /**
     * 查询外汇详情列表
     *
     * @param stForeignExchangenDetail 外汇详情
     * @return 外汇详情集合
     */
    public List<StForeignExchangenDetail> selectStForeignExchangenAndDetailAllInfo(StForeignExchangenDetail stForeignExchangenDetail){
        return  stForeignExchangenDetailMapper.selectStForeignExchangenAndDetailAllInfo(stForeignExchangenDetail);
    }

}
