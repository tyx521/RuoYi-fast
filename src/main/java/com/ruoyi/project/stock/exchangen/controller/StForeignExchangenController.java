package com.ruoyi.project.stock.exchangen.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.stock.exchangen.domain.StForeignExchangen;
import com.ruoyi.project.stock.exchangen.service.IStForeignExchangenService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 外汇Controller
 * 
 * @author ruoyi
 * @date 2019-08-19
 */
@Controller
@RequestMapping("/stock/exchangen")
public class StForeignExchangenController extends BaseController
{
    private String prefix = "stock/exchangen";

    @Autowired
    private IStForeignExchangenService stForeignExchangenService;

    @RequiresPermissions("stock:exchangen:view")
    @GetMapping()
    public String exchangen()
    {
        return prefix + "/exchangen";
    }

    /**
     * 查询外汇列表
     */
    @RequiresPermissions("stock:exchangen:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StForeignExchangen stForeignExchangen)
    {
        startPage();
        List<StForeignExchangen> list = stForeignExchangenService.selectStForeignExchangenList(stForeignExchangen);
        return getDataTable(list);
    }

    /**
     * 导出外汇列表
     */
    @RequiresPermissions("stock:exchangen:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StForeignExchangen stForeignExchangen)
    {
        List<StForeignExchangen> list = stForeignExchangenService.selectStForeignExchangenList(stForeignExchangen);
        ExcelUtil<StForeignExchangen> util = new ExcelUtil<StForeignExchangen>(StForeignExchangen.class);
        return util.exportExcel(list, "exchangen");
    }

    /**
     * 新增外汇
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存外汇
     */
    @RequiresPermissions("stock:exchangen:add")
    @Log(title = "外汇", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StForeignExchangen stForeignExchangen)
    {
        return toAjax(stForeignExchangenService.insertStForeignExchangen(stForeignExchangen));
    }

    /**
     * 修改外汇
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StForeignExchangen stForeignExchangen = stForeignExchangenService.selectStForeignExchangenById(id);
        mmap.put("stForeignExchangen", stForeignExchangen);
        return prefix + "/edit";
    }

    /**
     * 修改保存外汇
     */
    @RequiresPermissions("stock:exchangen:edit")
    @Log(title = "外汇", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StForeignExchangen stForeignExchangen)
    {
        return toAjax(stForeignExchangenService.updateStForeignExchangen(stForeignExchangen));
    }

    /**
     * 删除外汇
     */
    @RequiresPermissions("stock:exchangen:remove")
    @Log(title = "外汇", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stForeignExchangenService.deleteStForeignExchangenByIds(ids));
    }
}
