package com.ruoyi.project.stock.exchangen.mapper;

import com.ruoyi.project.stock.exchangen.domain.StForeignExchangenDetail;

import java.util.List;

/**
 * 外汇详情Mapper接口
 * 
 * @author ruoyi
 * @date 2019-08-19
 */
public interface StForeignExchangenDetailMapper 
{
    /**
     * 查询外汇详情
     * 
     * @param id 外汇详情ID
     * @return 外汇详情
     */
    public StForeignExchangenDetail selectStForeignExchangenDetailById(Long id);

    /**
     * 查询外汇详情列表
     * 
     * @param stForeignExchangenDetail 外汇详情
     * @return 外汇详情集合
     */
    public List<StForeignExchangenDetail> selectStForeignExchangenDetailList(StForeignExchangenDetail stForeignExchangenDetail);

    /**
     * 新增外汇详情
     * 
     * @param stForeignExchangenDetail 外汇详情
     * @return 结果
     */
    public int insertStForeignExchangenDetail(StForeignExchangenDetail stForeignExchangenDetail);

    /**
     * 修改外汇详情
     * 
     * @param stForeignExchangenDetail 外汇详情
     * @return 结果
     */
    public int updateStForeignExchangenDetail(StForeignExchangenDetail stForeignExchangenDetail);

    /**
     * 删除外汇详情
     * 
     * @param id 外汇详情ID
     * @return 结果
     */
    public int deleteStForeignExchangenDetailById(Long id);

    /**
     * 批量删除外汇详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStForeignExchangenDetailByIds(String[] ids);



    /**
     * 查询外汇详情列表
     *
     * @param stForeignExchangenDetail 外汇详情
     * @return 外汇详情集合
     */
    public List<StForeignExchangenDetail> selectStForeignExchangenAndDetailAllInfo(StForeignExchangenDetail stForeignExchangenDetail);

}
