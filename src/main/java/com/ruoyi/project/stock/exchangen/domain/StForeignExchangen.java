package com.ruoyi.project.stock.exchangen.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 外汇对象 st_foreign_exchangen
 * 
 * @author ruoyi
 * @date 2019-08-19
 */
public class StForeignExchangen extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 代码 */
    @Excel(name = "代码")
    private String exchangenCode;

    /** 名称 */
    @Excel(name = "名称")
    private String exchangenName;

    /** 类型（1：基本汇率 2：反向汇率 3：交叉汇率） */
    @Excel(name = "类型", readConverterExp = "1=：基本汇率,2=：反向汇率,3=：交叉汇率")
    private String exchangenType;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setExchangenCode(String exchangenCode) 
    {
        this.exchangenCode = exchangenCode;
    }

    public String getExchangenCode() 
    {
        return exchangenCode;
    }
    public void setExchangenName(String exchangenName) 
    {
        this.exchangenName = exchangenName;
    }

    public String getExchangenName() 
    {
        return exchangenName;
    }
    public void setExchangenType(String exchangenType) 
    {
        this.exchangenType = exchangenType;
    }

    public String getExchangenType() 
    {
        return exchangenType;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("exchangenCode", getExchangenCode())
            .append("exchangenName", getExchangenName())
            .append("exchangenType", getExchangenType())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
