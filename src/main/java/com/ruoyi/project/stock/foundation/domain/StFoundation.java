package com.ruoyi.project.stock.foundation.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 基金对象 st_foundation
 * 
 * @author ruoyi
 * @date 2019-08-15
 */
public class StFoundation extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 代码 */
    @Excel(name = "代码")
    private String foundCode;

    /** 名称 */
    @Excel(name = "名称")
    private String foundName;

    /** 类型（1：股票型 2：混合型 3：债券型 4：货币型 5：短期理财 6：QDII 7：保本型  8：指数型） */
    @Excel(name = "类型", readConverterExp = "1=：股票型,2=：混合型,3=：债券型,4=：货币型,5=：短期理财,6=：QDII,7=：保本型,8=：指数型")
    private String bondType;

    @Excel(name = "组织机构")
    private String organName;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFoundCode(String foundCode) 
    {
        this.foundCode = foundCode;
    }

    public String getFoundCode() 
    {
        return foundCode;
    }
    public void setFoundName(String foundName) 
    {
        this.foundName = foundName;
    }

    public String getFoundName() 
    {
        return foundName;
    }
    public void setBondType(String bondType) 
    {
        this.bondType = bondType;
    }

    public String getBondType() 
    {
        return bondType;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public String getOrganName() {
        return organName;
    }

    public void setOrganName(String organName) {
        this.organName = organName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("foundCode", getFoundCode())
                .append("foundName", getFoundName())
                .append("bondType", getBondType())
                .append("organName", getOrganName())
                .append("delFlag", getDelFlag())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .toString();
    }
}
