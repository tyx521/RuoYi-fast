package com.ruoyi.project.stock.foundation.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.stock.foundation.mapper.StFoundationMapper;
import com.ruoyi.project.stock.foundation.domain.StFoundation;
import com.ruoyi.project.stock.foundation.service.IStFoundationService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 基金Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-15
 */
@Service
public class StFoundationServiceImpl implements IStFoundationService 
{
    @Autowired
    private StFoundationMapper stFoundationMapper;

    /**
     * 查询基金
     * 
     * @param id 基金ID
     * @return 基金
     */
    @Override
    public StFoundation selectStFoundationById(Long id)
    {
        return stFoundationMapper.selectStFoundationById(id);
    }

    /**
     * 查询基金列表
     * 
     * @param stFoundation 基金
     * @return 基金
     */
    @Override
    public List<StFoundation> selectStFoundationList(StFoundation stFoundation)
    {
        return stFoundationMapper.selectStFoundationList(stFoundation);
    }

    /**
     * 新增基金
     * 
     * @param stFoundation 基金
     * @return 结果
     */
    @Override
    public int insertStFoundation(StFoundation stFoundation)
    {
        stFoundation.setCreateTime(DateUtils.getNowDate());
        return stFoundationMapper.insertStFoundation(stFoundation);
    }

    /**
     * 修改基金
     * 
     * @param stFoundation 基金
     * @return 结果
     */
    @Override
    public int updateStFoundation(StFoundation stFoundation)
    {
        stFoundation.setUpdateTime(DateUtils.getNowDate());
        return stFoundationMapper.updateStFoundation(stFoundation);
    }

    /**
     * 删除基金对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStFoundationByIds(String ids)
    {
        return stFoundationMapper.deleteStFoundationByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除基金信息
     * 
     * @param id 基金ID
     * @return 结果
     */
    public int deleteStFoundationById(Long id)
    {
        return stFoundationMapper.deleteStFoundationById(id);
    }

    /**
     * 查询基金
     *
     * @param id 基金编码
     * @return 基金
     */
    public StFoundation selectStFoundationByCode(String code){
        return  stFoundationMapper.selectStFoundationByCode(code);
    }
}
