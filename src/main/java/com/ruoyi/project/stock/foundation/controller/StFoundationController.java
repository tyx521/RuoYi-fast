package com.ruoyi.project.stock.foundation.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.stock.foundation.domain.StFoundation;
import com.ruoyi.project.stock.foundation.service.IStFoundationService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 基金Controller
 * 
 * @author ruoyi
 * @date 2019-08-15
 */
@Controller
@RequestMapping("/stock/foundation")
public class StFoundationController extends BaseController
{
    private String prefix = "stock/foundation";

    @Autowired
    private IStFoundationService stFoundationService;

    @RequiresPermissions("stock:foundation:view")
    @GetMapping()
    public String foundation()
    {
        return prefix + "/foundation";
    }

    /**
     * 查询基金列表
     */
    @RequiresPermissions("stock:foundation:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StFoundation stFoundation)
    {
        startPage();
        List<StFoundation> list = stFoundationService.selectStFoundationList(stFoundation);
        return getDataTable(list);
    }

    /**
     * 导出基金列表
     */
    @RequiresPermissions("stock:foundation:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StFoundation stFoundation)
    {
        List<StFoundation> list = stFoundationService.selectStFoundationList(stFoundation);
        ExcelUtil<StFoundation> util = new ExcelUtil<StFoundation>(StFoundation.class);
        return util.exportExcel(list, "foundation");
    }

    /**
     * 新增基金
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存基金
     */
    @RequiresPermissions("stock:foundation:add")
    @Log(title = "基金", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StFoundation stFoundation)
    {
        return toAjax(stFoundationService.insertStFoundation(stFoundation));
    }

    /**
     * 修改基金
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StFoundation stFoundation = stFoundationService.selectStFoundationById(id);
        mmap.put("stFoundation", stFoundation);
        return prefix + "/edit";
    }

    /**
     * 修改保存基金
     */
    @RequiresPermissions("stock:foundation:edit")
    @Log(title = "基金", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StFoundation stFoundation)
    {
        return toAjax(stFoundationService.updateStFoundation(stFoundation));
    }

    /**
     * 删除基金
     */
    @RequiresPermissions("stock:foundation:remove")
    @Log(title = "基金", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stFoundationService.deleteStFoundationByIds(ids));
    }
}
