package com.ruoyi.project.stock.foundation.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.stock.foundation.mapper.StFoundationDetailMapper;
import com.ruoyi.project.stock.foundation.domain.StFoundationDetail;
import com.ruoyi.project.stock.foundation.service.IStFoundationDetailService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 基金详情Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-15
 */
@Service
public class StFoundationDetailServiceImpl implements IStFoundationDetailService 
{
    @Autowired
    private StFoundationDetailMapper stFoundationDetailMapper;

    /**
     * 查询基金详情
     * 
     * @param id 基金详情ID
     * @return 基金详情
     */
    @Override
    public StFoundationDetail selectStFoundationDetailById(Long id)
    {
        return stFoundationDetailMapper.selectStFoundationDetailById(id);
    }

    /**
     * 查询基金详情列表
     * 
     * @param stFoundationDetail 基金详情
     * @return 基金详情
     */
    @Override
    public List<StFoundationDetail> selectStFoundationDetailList(StFoundationDetail stFoundationDetail)
    {
        return stFoundationDetailMapper.selectStFoundationDetailList(stFoundationDetail);
    }

    /**
     * 新增基金详情
     * 
     * @param stFoundationDetail 基金详情
     * @return 结果
     */
    @Override
    public int insertStFoundationDetail(StFoundationDetail stFoundationDetail)
    {
        stFoundationDetail.setCreateTime(DateUtils.getNowDate());
        return stFoundationDetailMapper.insertStFoundationDetail(stFoundationDetail);
    }

    /**
     * 修改基金详情
     * 
     * @param stFoundationDetail 基金详情
     * @return 结果
     */
    @Override
    public int updateStFoundationDetail(StFoundationDetail stFoundationDetail)
    {
        stFoundationDetail.setUpdateTime(DateUtils.getNowDate());
        return stFoundationDetailMapper.updateStFoundationDetail(stFoundationDetail);
    }

    /**
     * 删除基金详情对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStFoundationDetailByIds(String ids)
    {
        return stFoundationDetailMapper.deleteStFoundationDetailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除基金详情信息
     * 
     * @param id 基金详情ID
     * @return 结果
     */
    public int deleteStFoundationDetailById(Long id)
    {
        return stFoundationDetailMapper.deleteStFoundationDetailById(id);
    }

    /**
     * 查询基金详情列表
     *
     * @param stFoundationDetail 基金详情
     * @return 基金详情集合
     */
    public List<StFoundationDetail> selectFoundationAndDetailAllInfoList(StFoundationDetail stFoundationDetail){
        return  stFoundationDetailMapper.selectFoundationAndDetailAllInfoList(stFoundationDetail);
    }
}
