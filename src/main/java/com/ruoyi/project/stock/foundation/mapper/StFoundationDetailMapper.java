package com.ruoyi.project.stock.foundation.mapper;

import com.ruoyi.project.stock.foundation.domain.StFoundationDetail;
import java.util.List;

/**
 * 基金详情Mapper接口
 * 
 * @author ruoyi
 * @date 2019-08-15
 */
public interface StFoundationDetailMapper 
{
    /**
     * 查询基金详情
     * 
     * @param id 基金详情ID
     * @return 基金详情
     */
    public StFoundationDetail selectStFoundationDetailById(Long id);

    /**
     * 查询基金详情列表
     * 
     * @param stFoundationDetail 基金详情
     * @return 基金详情集合
     */
    public List<StFoundationDetail> selectStFoundationDetailList(StFoundationDetail stFoundationDetail);

    /**
     * 新增基金详情
     * 
     * @param stFoundationDetail 基金详情
     * @return 结果
     */
    public int insertStFoundationDetail(StFoundationDetail stFoundationDetail);

    /**
     * 修改基金详情
     * 
     * @param stFoundationDetail 基金详情
     * @return 结果
     */
    public int updateStFoundationDetail(StFoundationDetail stFoundationDetail);

    /**
     * 删除基金详情
     * 
     * @param id 基金详情ID
     * @return 结果
     */
    public int deleteStFoundationDetailById(Long id);

    /**
     * 批量删除基金详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStFoundationDetailByIds(String[] ids);



    /**
     * 查询基金详情列表
     *
     * @param stFoundationDetail 基金详情
     * @return 基金详情集合
     */
    public List<StFoundationDetail> selectFoundationAndDetailAllInfoList(StFoundationDetail stFoundationDetail);


}
