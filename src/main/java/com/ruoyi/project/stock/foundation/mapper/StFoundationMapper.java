package com.ruoyi.project.stock.foundation.mapper;

import com.ruoyi.project.stock.foundation.domain.StFoundation;
import java.util.List;

/**
 * 基金Mapper接口
 * 
 * @author ruoyi
 * @date 2019-08-15
 */
public interface StFoundationMapper 
{
    /**
     * 查询基金
     * 
     * @param id 基金ID
     * @return 基金
     */
    public StFoundation selectStFoundationById(Long id);

    /**
     * 查询基金列表
     * 
     * @param stFoundation 基金
     * @return 基金集合
     */
    public List<StFoundation> selectStFoundationList(StFoundation stFoundation);

    /**
     * 新增基金
     * 
     * @param stFoundation 基金
     * @return 结果
     */
    public int insertStFoundation(StFoundation stFoundation);

    /**
     * 修改基金
     * 
     * @param stFoundation 基金
     * @return 结果
     */
    public int updateStFoundation(StFoundation stFoundation);

    /**
     * 删除基金
     * 
     * @param id 基金ID
     * @return 结果
     */
    public int deleteStFoundationById(Long id);

    /**
     * 批量删除基金
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStFoundationByIds(String[] ids);

    /**
     * 查询基金
     *
     * @param id 基金编码
     * @return 基金
     */
    public StFoundation selectStFoundationByCode(String code);
}
