package com.ruoyi.project.stock.foundation.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.stock.foundation.domain.StFoundationDetail;
import com.ruoyi.project.stock.foundation.service.IStFoundationDetailService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;
import java.util.List;

/**
 * 基金详情Controller
 * 
 * @author ruoyi
 * @date 2019-08-15
 */
@Controller
@RequestMapping("/stock/foundation/detail")
public class StFoundationDetailController extends BaseController
{
    private String prefix = "stock/foundation/detail";

    @Autowired
    private IStFoundationDetailService stFoundationDetailService;

    @RequiresPermissions("stock:foundation:detail:view")
    @GetMapping()
    public String detail()
    {
        return prefix + "/detail";
    }

    /**
     * 查询基金详情列表
     */
    @RequiresPermissions("stock:foundation:detail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StFoundationDetail stFoundationDetail)
    {
        startPage();
        //List<StFoundationDetail> list = stFoundationDetailService.selectStFoundationDetailList(stFoundationDetail);
        List<StFoundationDetail> list = stFoundationDetailService.selectFoundationAndDetailAllInfoList(stFoundationDetail);
        return getDataTable(list);
    }

    /**
     * 导出基金详情列表
     */
    @RequiresPermissions("stock:foundation:detail:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StFoundationDetail stFoundationDetail)
    {
        List<StFoundationDetail> list = stFoundationDetailService.selectStFoundationDetailList(stFoundationDetail);
        ExcelUtil<StFoundationDetail> util = new ExcelUtil<StFoundationDetail>(StFoundationDetail.class);
        return util.exportExcel(list, "detail");
    }

    /**
     * 新增基金详情
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存基金详情
     */
    @RequiresPermissions("stock:foundation:detail:add")
    @Log(title = "基金详情", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StFoundationDetail stFoundationDetail)
    {
        return toAjax(stFoundationDetailService.insertStFoundationDetail(stFoundationDetail));
    }

    /**
     * 修改基金详情
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StFoundationDetail stFoundationDetail = stFoundationDetailService.selectStFoundationDetailById(id);
        mmap.put("stFoundationDetail", stFoundationDetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存基金详情
     */
    @RequiresPermissions("stock:foundation:detail:edit")
    @Log(title = "基金详情", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StFoundationDetail stFoundationDetail)
    {
        return toAjax(stFoundationDetailService.updateStFoundationDetail(stFoundationDetail));
    }

    /**
     * 删除基金详情
     */
    @RequiresPermissions("stock:foundation:detail:remove")
    @Log(title = "基金详情", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stFoundationDetailService.deleteStFoundationDetailByIds(ids));
    }
}
