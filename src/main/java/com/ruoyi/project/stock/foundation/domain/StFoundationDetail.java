package com.ruoyi.project.stock.foundation.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 基金详情对象 st_foundation_detail
 * 
 * @author ruoyi
 * @date 2019-08-15
 */
public class StFoundationDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 基金id */
    @Excel(name = "基金id")
    private Long foundId;

    /** 日期 */
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dataTime;

    /** 净值 */
    @Excel(name = "净值")
    private Double netValue;

    /** 累计净值 */
    @Excel(name = "累计净值")
    private Double totalNetValue;

    /** 日增长率 */
    @Excel(name = "日增长率")
    private Double dayPercent;

    /** 近一周 */
    @Excel(name = "近一周")
    private Double weekPercent;

    /** 近一月 */
    @Excel(name = "近一月")
    private Double monthPercent;

    /** 近三月 */
    @Excel(name = "近三月")
    private Double threeMonthPercent;

    /** 近一年 */
    @Excel(name = "近一年")
    private Double yearPercent;

    /** 成立以来 */
    @Excel(name = "成立以来")
    private Double establishPercent;


    private StFoundation foundation;

    public StFoundation getFoundation() {
        return foundation;
    }

    public void setFoundation(StFoundation foundation) {
        this.foundation = foundation;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFoundId(Long foundId) 
    {
        this.foundId = foundId;
    }

    public Long getFoundId() 
    {
        return foundId;
    }
    public void setDataTime(Date dataTime) 
    {
        this.dataTime = dataTime;
    }

    public Date getDataTime() 
    {
        return dataTime;
    }
    public void setNetValue(Double netValue) 
    {
        this.netValue = netValue;
    }

    public Double getNetValue() 
    {
        return netValue;
    }
    public void setTotalNetValue(Double totalNetValue) 
    {
        this.totalNetValue = totalNetValue;
    }

    public Double getTotalNetValue() 
    {
        return totalNetValue;
    }
    public void setDayPercent(Double dayPercent) 
    {
        this.dayPercent = dayPercent;
    }

    public Double getDayPercent() 
    {
        return dayPercent;
    }
    public void setWeekPercent(Double weekPercent) 
    {
        this.weekPercent = weekPercent;
    }

    public Double getWeekPercent() 
    {
        return weekPercent;
    }
    public void setMonthPercent(Double monthPercent) 
    {
        this.monthPercent = monthPercent;
    }

    public Double getMonthPercent() 
    {
        return monthPercent;
    }
    public void setThreeMonthPercent(Double threeMonthPercent) 
    {
        this.threeMonthPercent = threeMonthPercent;
    }

    public Double getThreeMonthPercent() 
    {
        return threeMonthPercent;
    }
    public void setYearPercent(Double yearPercent) 
    {
        this.yearPercent = yearPercent;
    }

    public Double getYearPercent() 
    {
        return yearPercent;
    }
    public void setEstablishPercent(Double establishPercent) 
    {
        this.establishPercent = establishPercent;
    }

    public Double getEstablishPercent() 
    {
        return establishPercent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("foundId", getFoundId())
            .append("dataTime", getDataTime())
            .append("netValue", getNetValue())
            .append("totalNetValue", getTotalNetValue())
            .append("dayPercent", getDayPercent())
            .append("weekPercent", getWeekPercent())
            .append("monthPercent", getMonthPercent())
            .append("threeMonthPercent", getThreeMonthPercent())
            .append("yearPercent", getYearPercent())
            .append("establishPercent", getEstablishPercent())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
