package com.ruoyi.project.stock.bond.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.stock.bond.mapper.StBondDetailMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.stock.bond.domain.StBondDetail;
import com.ruoyi.project.stock.bond.service.IStBondDetailService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 债券详情Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-13
 */
@Service
public class StBondDetailServiceImpl implements IStBondDetailService 
{
    @Autowired
    private StBondDetailMapper stBondDetailMapper;

    /**
     * 查询债券详情
     * 
     * @param id 债券详情ID
     * @return 债券详情
     */
    @Override
    public StBondDetail selectStBondDetailById(Long id)
    {
        return stBondDetailMapper.selectStBondDetailById(id);
    }

    /**
     * 查询债券详情列表
     * 
     * @param stBondDetail 债券详情
     * @return 债券详情
     */
    @Override
    public List<StBondDetail> selectStBondDetailList(StBondDetail stBondDetail)
    {
        return stBondDetailMapper.selectStBondDetailList(stBondDetail);
    }

    /**
     * 新增债券详情
     * 
     * @param stBondDetail 债券详情
     * @return 结果
     */
    @Override
    public int insertStBondDetail(StBondDetail stBondDetail)
    {
        stBondDetail.setCreateTime(DateUtils.getNowDate());
        return stBondDetailMapper.insertStBondDetail(stBondDetail);
    }

    /**
     * 修改债券详情
     * 
     * @param stBondDetail 债券详情
     * @return 结果
     */
    @Override
    public int updateStBondDetail(StBondDetail stBondDetail)
    {
        stBondDetail.setUpdateTime(DateUtils.getNowDate());
        return stBondDetailMapper.updateStBondDetail(stBondDetail);
    }

    /**
     * 删除债券详情对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStBondDetailByIds(String ids)
    {
        return stBondDetailMapper.deleteStBondDetailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除债券详情信息
     * 
     * @param id 债券详情ID
     * @return 结果
     */
    public int deleteStBondDetailById(Long id)
    {
        return stBondDetailMapper.deleteStBondDetailById(id);
    }

    @Override
    public List<StBondDetail> selectBondAndDetailAllInfoList(StBondDetail stBondDetail) {
        return stBondDetailMapper.selectBondAndDetailAllInfoList(stBondDetail);
    }
}
