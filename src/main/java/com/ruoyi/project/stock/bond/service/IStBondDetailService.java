package com.ruoyi.project.stock.bond.service;

import com.ruoyi.project.stock.bond.domain.StBondDetail;
import java.util.List;

/**
 * 债券详情Service接口
 * 
 * @author ruoyi
 * @date 2019-08-13
 */
public interface IStBondDetailService 
{
    /**
     * 查询债券详情
     * 
     * @param id 债券详情ID
     * @return 债券详情
     */
    public StBondDetail selectStBondDetailById(Long id);

    /**
     * 查询债券详情列表
     * 
     * @param stBondDetail 债券详情
     * @return 债券详情集合
     */
    public List<StBondDetail> selectStBondDetailList(StBondDetail stBondDetail);

    /**
     * 新增债券详情
     * 
     * @param stBondDetail 债券详情
     * @return 结果
     */
    public int insertStBondDetail(StBondDetail stBondDetail);

    /**
     * 修改债券详情
     * 
     * @param stBondDetail 债券详情
     * @return 结果
     */
    public int updateStBondDetail(StBondDetail stBondDetail);

    /**
     * 批量删除债券详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStBondDetailByIds(String ids);

    /**
     * 删除债券详情信息
     * 
     * @param id 债券详情ID
     * @return 结果
     */
    public int deleteStBondDetailById(Long id);

    /**
     * 根据st_bond 和st_bond_detail进行左连接查询
     * @param stBondDetail
     * @return
     */
    public List<StBondDetail> selectBondAndDetailAllInfoList(StBondDetail stBondDetail);
}
