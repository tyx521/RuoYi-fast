package com.ruoyi.project.stock.bond.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.stock.bond.mapper.StBondMapper;
import com.ruoyi.project.stock.bond.domain.StBond;
import com.ruoyi.project.stock.bond.service.IStBondService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 债券Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-12
 */
@Service
public class StBondServiceImpl implements IStBondService 
{
    @Autowired
    private StBondMapper stBondMapper;

    /**
     * 查询债券
     * 
     * @param bondId 债券ID
     * @return 债券
     */
    @Override
    public StBond selectStBondById(Long bondId)
    {
        return stBondMapper.selectStBondById(bondId);
    }

    /**
     * 查询债券列表
     * 
     * @param stBond 债券
     * @return 债券
     */
    @Override
    public List<StBond> selectStBondList(StBond stBond)
    {
        return stBondMapper.selectStBondList(stBond);
    }

    /**
     * 新增债券
     * 
     * @param stBond 债券
     * @return 结果
     */
    @Override
    public int insertStBond(StBond stBond)
    {
        stBond.setCreateTime(DateUtils.getNowDate());
        return stBondMapper.insertStBond(stBond);
    }

    /**
     * 修改债券
     * 
     * @param stBond 债券
     * @return 结果
     */
    @Override
    public int updateStBond(StBond stBond)
    {
        stBond.setUpdateTime(DateUtils.getNowDate());
        return stBondMapper.updateStBond(stBond);
    }

    /**
     * 删除债券对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStBondByIds(String ids)
    {
        return stBondMapper.deleteStBondByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除债券信息
     * 
     * @param bondId 债券ID
     * @return 结果
     */
    public int deleteStBondById(Long bondId)
    {
        return stBondMapper.deleteStBondById(bondId);
    }


    /**
     * 查询债券
     *
     * @param bondId 债券编码
     * @return 债券
     */
    public StBond selectStBondByCode(String code){return stBondMapper.selectStBondByCode(code);}
}
