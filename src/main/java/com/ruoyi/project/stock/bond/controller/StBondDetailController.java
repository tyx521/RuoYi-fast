package com.ruoyi.project.stock.bond.controller;

import java.util.List;

import com.ruoyi.project.stock.bond.domain.StBondDetail;
import com.ruoyi.project.stock.bond.service.IStBondDetailService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 债券详情Controller
 * 
 * @author ruoyi
 * @date 2019-08-13
 */
@Controller
@RequestMapping("/stock/bondDetail")
public class StBondDetailController extends BaseController
{
    private String prefix = "stock/bond/detail";

    @Autowired
    private IStBondDetailService stBondDetailService;

    @RequiresPermissions("stock:bond:detail:view")
    @GetMapping()
    public String detail()
    {
        return prefix + "/detail";
    }

    /**
     * 查询债券详情列表
     */
    @RequiresPermissions("stock:bond:detail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StBondDetail stBondDetail)
    {
        startPage();
        //List<StBondDetail> list = stBondDetailService.selectStBondDetailList(stBondDetail);
        List<StBondDetail> list = stBondDetailService.selectBondAndDetailAllInfoList(stBondDetail);
        return getDataTable(list);
    }

    /**
     * 导出债券详情列表
     */
    @RequiresPermissions("stock:bond:detail:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StBondDetail stBondDetail)
    {
        List<StBondDetail> list = stBondDetailService.selectStBondDetailList(stBondDetail);
        ExcelUtil<StBondDetail> util = new ExcelUtil<StBondDetail>(StBondDetail.class);
        return util.exportExcel(list, "detail");
    }

    /**
     * 新增债券详情
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存债券详情
     */
    @RequiresPermissions("stock:bond:detail:add")
    @Log(title = "债券详情", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StBondDetail stBondDetail)
    {
        return toAjax(stBondDetailService.insertStBondDetail(stBondDetail));
    }

    /**
     * 修改债券详情
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StBondDetail stBondDetail = stBondDetailService.selectStBondDetailById(id);
        mmap.put("stBondDetail", stBondDetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存债券详情
     */
    @RequiresPermissions("stock:bond:detail:edit")
    @Log(title = "债券详情", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StBondDetail stBondDetail)
    {
        return toAjax(stBondDetailService.updateStBondDetail(stBondDetail));
    }

    /**
     * 删除债券详情
     */
    @RequiresPermissions("stock:bond:detail:remove")
    @Log(title = "债券详情", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stBondDetailService.deleteStBondDetailByIds(ids));
    }
}
