package com.ruoyi.project.stock.bond.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 债券对象 st_bond
 * 
 * @author ruoyi
 * @date 2019-08-12
 */
public class StBond extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 债券id */
    private Long bondId;

    /** 债券编码 */
    @Excel(name = "债券编码")
    private String bondCode;

    /** 债券名称 */
    @Excel(name = "债券名称")
    private String bondName;

    /** 债券类型（1：地方债 2：国债 3：回购 4：可转债 5：企业债 6：贴债） */
    @Excel(name = "债券类型", readConverterExp = "1=：地方债,2=：国债,3=：回购,4=：可转债,5=：企业债,6=：贴债")
    private String bondType;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setBondId(Long bondId) 
    {
        this.bondId = bondId;
    }

    public Long getBondId() 
    {
        return bondId;
    }
    public void setBondCode(String bondCode) 
    {
        this.bondCode = bondCode;
    }

    public String getBondCode() 
    {
        return bondCode;
    }
    public void setBondName(String bondName) 
    {
        this.bondName = bondName;
    }

    public String getBondName() 
    {
        return bondName;
    }
    public void setBondType(String bondType) 
    {
        this.bondType = bondType;
    }

    public String getBondType() 
    {
        return bondType;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("bondId", getBondId())
            .append("bondCode", getBondCode())
            .append("bondName", getBondName())
            .append("bondType", getBondType())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
