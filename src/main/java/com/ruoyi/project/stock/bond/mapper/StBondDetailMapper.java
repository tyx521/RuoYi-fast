package com.ruoyi.project.stock.bond.mapper;

import java.util.List;
import com.ruoyi.project.stock.bond.domain.StBondDetail;

/**
 * 债券详情Mapper接口
 * 
 * @author ruoyi
 * @date 2019-08-13
 */
public interface StBondDetailMapper 
{
    /**
     * 查询债券详情
     * 
     * @param id 债券详情ID
     * @return 债券详情
     */
    public StBondDetail selectStBondDetailById(Long id);

    /**
     * 查询债券详情列表
     * 
     * @param stBondDetail 债券详情
     * @return 债券详情集合
     */
    public List<StBondDetail> selectStBondDetailList(StBondDetail stBondDetail);

    /**
     * 新增债券详情
     * 
     * @param stBondDetail 债券详情
     * @return 结果
     */
    public int insertStBondDetail(StBondDetail stBondDetail);

    /**
     * 修改债券详情
     * 
     * @param stBondDetail 债券详情
     * @return 结果
     */
    public int updateStBondDetail(StBondDetail stBondDetail);

    /**
     * 删除债券详情
     * 
     * @param id 债券详情ID
     * @return 结果
     */
    public int deleteStBondDetailById(Long id);

    /**
     * 批量删除债券详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStBondDetailByIds(String[] ids);



    /**
     * 查询债券详情列表
     *
     * @param stBondDetail 债券详情
     * @return 债券详情集合
     */
    public List<StBondDetail> selectBondAndDetailAllInfoList(StBondDetail stBondDetail);
}
