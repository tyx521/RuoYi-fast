package com.ruoyi.project.stock.bond.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

import java.util.Date;

/**
 * 债券详情对象 st_bond_detail
 *
 * @author ruoyi
 * @date 2019-08-13
 */
public class StBondDetail extends BaseEntity {
    private static final long serialVersionUID = 1L;



    /**
     * 主键id
     */
    private Long id;

    /**
     * 债券id
     */
    @Excel(name = "债券id")
    private Long bondId;

    /**
     * 日期
     */
    @Excel(name = "日期")
    private Date dataTime;

    /**
     * 利率
     */
    @Excel(name = "利率")
    private Double interest;

    /**
     * 利息
     */
    @Excel(name = "利息")
    private Double rates;

    /**
     * 涨幅%
     */
    @Excel(name = "涨幅%")
    private Double upPercent;

    /**
     * 现价
     */
    @Excel(name = "现价")
    private Double nowPrice;

    /**
     * 总手
     */
    @Excel(name = "总手")
    private Long totalHands;

    /**
     * 昨收
     */
    @Excel(name = "昨收")
    private Double prevClose;

    /**
     * 开盘
     */
    @Excel(name = "开盘")
    private Double openPrice;

    /**
     * 最高价
     */
    @Excel(name = "最高价")
    private Double highestPrice;

    /**
     * 最低价
     */
    @Excel(name = "最低价")
    private Double lowestPrice;

    /**
     * 买价
     */
    @Excel(name = "买价")
    private Double buyPrice;

    /**
     * 卖价
     */
    @Excel(name = "卖价")
    private Double sellPrice;

    /**
     * 总金额
     */
    @Excel(name = "总金额")
    private Long totalAmount;

    /**
     * 买量
     */
    @Excel(name = "买量")
    private Long buyVolume;



    /**
     * 卖量
     */
    @Excel(name = "卖量")
    private Long sellVolume;

    /**
     * 债券对象， 用于关联查询
     */
    private StBond bond;

    public StBond getBond() {
        return bond;
    }

    public void setBond(StBond bond) {
        this.bond = bond;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setBondId(Long bondId) {
        this.bondId = bondId;
    }

    public Long getBondId() {
        return bondId;
    }

    public void setInterest(Double interest) {
        this.interest = interest;
    }

    public Double getInterest() {
        return interest;
    }

    public void setRates(Double rates) {
        this.rates = rates;
    }

    public Double getRates() {
        return rates;
    }

    public void setUpPercent(Double upPercent) {
        this.upPercent = upPercent;
    }

    public Double getUpPercent() {
        return upPercent;
    }

    public void setNowPrice(Double nowPrice) {
        this.nowPrice = nowPrice;
    }

    public Double getNowPrice() {
        return nowPrice;
    }

    public void setTotalHands(Long totalHands) {
        this.totalHands = totalHands;
    }

    public Long getTotalHands() {
        return totalHands;
    }

    public void setPrevClose(Double prevClose) {
        this.prevClose = prevClose;
    }

    public Double getPrevClose() {
        return prevClose;
    }

    public void setOpenPrice(Double openPrice) {
        this.openPrice = openPrice;
    }

    public Double getOpenPrice() {
        return openPrice;
    }

    public void setHighestPrice(Double highestPrice) {
        this.highestPrice = highestPrice;
    }

    public Double getHighestPrice() {
        return highestPrice;
    }

    public void setLowestPrice(Double lowestPrice) {
        this.lowestPrice = lowestPrice;
    }

    public Double getLowestPrice() {
        return lowestPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setSellPrice(Double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Double getSellPrice() {
        return sellPrice;
    }

    public void setTotalAmount(Long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Long getTotalAmount() {
        return totalAmount;
    }

    public void setBuyVolume(Long buyVolume) {
        this.buyVolume = buyVolume;
    }

    public Long getBuyVolume() {
        return buyVolume;
    }

    public void setSellVolume(Long sellVolume) {
        this.sellVolume = sellVolume;
    }

    public Long getSellVolume() {
        return sellVolume;
    }

    public Date getDataTime() {
        return dataTime;
    }

    public void setDataTime(Date dataTime) {
        this.dataTime = dataTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("bondId", getBondId())
                .append("interest", getInterest())
                .append("rates", getRates())
                .append("upPercent", getUpPercent())
                .append("nowPrice", getNowPrice())
                .append("totalHands", getTotalHands())
                .append("prevClose", getPrevClose())
                .append("openPrice", getOpenPrice())
                .append("highestPrice", getHighestPrice())
                .append("lowestPrice", getLowestPrice())
                .append("buyPrice", getBuyPrice())
                .append("sellPrice", getSellPrice())
                .append("totalAmount", getTotalAmount())
                .append("buyVolume", getBuyVolume())
                .append("sellVolume", getSellVolume())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("dataTime", getDataTime())
                .toString();
    }
}
