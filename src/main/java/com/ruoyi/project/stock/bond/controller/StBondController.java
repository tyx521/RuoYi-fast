package com.ruoyi.project.stock.bond.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.stock.bond.domain.StBond;
import com.ruoyi.project.stock.bond.service.IStBondService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 债券Controller
 * 
 * @author ruoyi
 * @date 2019-08-12
 */
@Controller
@RequestMapping("/stock/bond")
public class StBondController extends BaseController
{
    private String prefix = "stock/bond";

    @Autowired
    private IStBondService stBondService;

    @RequiresPermissions("stock:bond:view")
    @GetMapping()
    public String bond()
    {
        return prefix + "/bond";
    }

    /**
     * 查询债券列表
     */
    @RequiresPermissions("stock:bond:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StBond stBond)
    {
        startPage();
        List<StBond> list = stBondService.selectStBondList(stBond);
        return getDataTable(list);
    }

    /**
     * 导出债券列表
     */
    @RequiresPermissions("stock:bond:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StBond stBond)
    {
        List<StBond> list = stBondService.selectStBondList(stBond);
        ExcelUtil<StBond> util = new ExcelUtil<StBond>(StBond.class);
        return util.exportExcel(list, "bond");
    }

    /**
     * 新增债券
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存债券
     */
    @RequiresPermissions("stock:bond:add")
    @Log(title = "债券", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StBond stBond)
    {
        return toAjax(stBondService.insertStBond(stBond));
    }

    /**
     * 修改债券
     */
    @GetMapping("/edit/{bondId}")
    public String edit(@PathVariable("bondId") Long bondId, ModelMap mmap)
    {
        StBond stBond = stBondService.selectStBondById(bondId);
        mmap.put("stBond", stBond);
        return prefix + "/edit";
    }

    /**
     * 修改保存债券
     */
    @RequiresPermissions("stock:bond:edit")
    @Log(title = "债券", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StBond stBond)
    {
        return toAjax(stBondService.updateStBond(stBond));
    }

    /**
     * 删除债券
     */
    @RequiresPermissions("stock:bond:remove")
    @Log(title = "债券", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stBondService.deleteStBondByIds(ids));
    }
}
