package com.ruoyi.project.stock.bond.service;

import com.ruoyi.project.stock.bond.domain.StBond;
import java.util.List;

/**
 * 债券Service接口
 * 
 * @author ruoyi
 * @date 2019-08-12
 */
public interface IStBondService 
{
    /**
     * 查询债券
     * 
     * @param bondId 债券ID
     * @return 债券
     */
    public StBond selectStBondById(Long bondId);

    /**
     * 查询债券列表
     * 
     * @param stBond 债券
     * @return 债券集合
     */
    public List<StBond> selectStBondList(StBond stBond);

    /**
     * 新增债券
     * 
     * @param stBond 债券
     * @return 结果
     */
    public int insertStBond(StBond stBond);

    /**
     * 修改债券
     * 
     * @param stBond 债券
     * @return 结果
     */
    public int updateStBond(StBond stBond);

    /**
     * 批量删除债券
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStBondByIds(String ids);

    /**
     * 删除债券信息
     * 
     * @param bondId 债券ID
     * @return 结果
     */
    public int deleteStBondById(Long bondId);


    /**
     * 查询债券
     *
     * @param bondId 债券编码
     * @return 债券
     */
    public StBond selectStBondByCode(String code);
}
