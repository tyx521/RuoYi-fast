package com.ruoyi.project.stock.futures.service;


import java.util.List;
import com.ruoyi.framework.web.domain.Ztree;
import com.ruoyi.project.stock.futures.domain.StFuturesCategory;

/**
 * 期货分类Service接口
 * 
 * @author ruoyi
 * @date 2019-08-20
 */
public interface IStFuturesCategoryService 
{
    /**
     * 查询期货分类
     * 
     * @param id 期货分类ID
     * @return 期货分类
     */
    public StFuturesCategory selectStFuturesCategoryById(Long id);

    /**
     * 查询期货分类列表
     * 
     * @param stFuturesCategory 期货分类
     * @return 期货分类集合
     */
    public List<StFuturesCategory> selectStFuturesCategoryList(StFuturesCategory stFuturesCategory);

    /**
     * 新增期货分类
     * 
     * @param stFuturesCategory 期货分类
     * @return 结果
     */
    public int insertStFuturesCategory(StFuturesCategory stFuturesCategory);

    /**
     * 修改期货分类
     * 
     * @param stFuturesCategory 期货分类
     * @return 结果
     */
    public int updateStFuturesCategory(StFuturesCategory stFuturesCategory);

    /**
     * 批量删除期货分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStFuturesCategoryByIds(String ids);

    /**
     * 删除期货分类信息
     * 
     * @param id 期货分类ID
     * @return 结果
     */
    public int deleteStFuturesCategoryById(Long id);

    /**
     * 查询期货分类树列表
     * 
     * @return 所有期货分类信息
     */
    public List<Ztree> selectStFuturesCategoryTree();
}
