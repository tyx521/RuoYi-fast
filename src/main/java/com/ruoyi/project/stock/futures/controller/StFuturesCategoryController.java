package com.ruoyi.project.stock.futures.controller;

import java.util.List;
import com.ruoyi.project.stock.futures.domain.StFuturesCategory;
import com.ruoyi.project.stock.futures.service.IStFuturesCategoryService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.framework.web.domain.Ztree;

/**
 * 期货分类Controller
 * 
 * @author ruoyi
 * @date 2019-08-20
 */
@Controller
@RequestMapping("/stock/futurescategory")
public class StFuturesCategoryController extends BaseController
{
    private String prefix = "stock/futures/futurescategory";

    @Autowired
    private IStFuturesCategoryService stFuturesCategoryService;

    @RequiresPermissions("stock:futurescategory:view")
    @GetMapping()
    public String futurescategory()
    {
        return prefix + "/futurescategory";
    }

    /**
     * 查询期货分类树列表
     */
    @RequiresPermissions("stock:futurescategory:list")
    @PostMapping("/list")
    @ResponseBody
    public List<StFuturesCategory> list(StFuturesCategory stFuturesCategory)
    {
        List<StFuturesCategory> list = stFuturesCategoryService.selectStFuturesCategoryList(stFuturesCategory);
        return list;
    }

    /**
     * 导出期货分类列表
     */
    @RequiresPermissions("stock:futurescategory:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StFuturesCategory stFuturesCategory)
    {
        List<StFuturesCategory> list = stFuturesCategoryService.selectStFuturesCategoryList(stFuturesCategory);
        ExcelUtil<StFuturesCategory> util = new ExcelUtil<StFuturesCategory>(StFuturesCategory.class);
        return util.exportExcel(list, "futurescategory");
    }

    /**
     * 新增期货分类
     */
    @GetMapping(value = { "/add/{id}", "/add/" })
    public String add(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("stFuturesCategory", stFuturesCategoryService.selectStFuturesCategoryById(id));
        }
        return prefix + "/add";
    }

    /**
     * 新增保存期货分类
     */
    @RequiresPermissions("stock:futurescategory:add")
    @Log(title = "期货分类", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StFuturesCategory stFuturesCategory)
    {
        return toAjax(stFuturesCategoryService.insertStFuturesCategory(stFuturesCategory));
    }

    /**
     * 修改期货分类
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StFuturesCategory stFuturesCategory = stFuturesCategoryService.selectStFuturesCategoryById(id);
        mmap.put("stFuturesCategory", stFuturesCategory);
        return prefix + "/edit";
    }

    /**
     * 修改保存期货分类
     */
    @RequiresPermissions("stock:futurescategory:edit")
    @Log(title = "期货分类", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StFuturesCategory stFuturesCategory)
    {
        return toAjax(stFuturesCategoryService.updateStFuturesCategory(stFuturesCategory));
    }

    /**
     * 删除
     */
    @RequiresPermissions("stock:futurescategory:remove")
    @Log(title = "期货分类", businessType = BusinessType.DELETE)
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") Long id)
    {
        return toAjax(stFuturesCategoryService.deleteStFuturesCategoryById(id));
    }

    /**
     * 选择期货分类树
     */
    @GetMapping(value = { "/selectFuturescategoryTree/{id}", "/selectFuturescategoryTree/" })
    public String selectFuturescategoryTree(@PathVariable(value = "id", required = false) Long id, ModelMap mmap)
    {
        if (StringUtils.isNotNull(id))
        {
            mmap.put("stFuturesCategory", stFuturesCategoryService.selectStFuturesCategoryById(id));
        }
        return prefix + "/tree";
    }

    /**
     * 加载期货分类树列表
     */
    @GetMapping("/treeData")
    @ResponseBody
    public List<Ztree> treeData()
    {
        List<Ztree> ztrees = stFuturesCategoryService.selectStFuturesCategoryTree();
        return ztrees;
    }
}
