package com.ruoyi.project.stock.futures.service.impl;

import java.util.List;
import java.util.ArrayList;
import com.ruoyi.framework.web.domain.Ztree;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.stock.futures.domain.StFuturesCategory;
import com.ruoyi.project.stock.futures.mapper.StFuturesCategoryMapper;
import com.ruoyi.project.stock.futures.service.IStFuturesCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 期货分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2019-08-20
 */
@Service
public class StFuturesCategoryServiceImpl implements IStFuturesCategoryService
{
    @Autowired
    private StFuturesCategoryMapper stFuturesCategoryMapper;

    /**
     * 查询期货分类
     * 
     * @param id 期货分类ID
     * @return 期货分类
     */
    @Override
    public StFuturesCategory selectStFuturesCategoryById(Long id)
    {
        return stFuturesCategoryMapper.selectStFuturesCategoryById(id);
    }

    /**
     * 查询期货分类列表
     * 
     * @param stFuturesCategory 期货分类
     * @return 期货分类
     */
    @Override
    public List<StFuturesCategory> selectStFuturesCategoryList(StFuturesCategory stFuturesCategory)
    {
        return stFuturesCategoryMapper.selectStFuturesCategoryList(stFuturesCategory);
    }

    /**
     * 新增期货分类
     * 
     * @param stFuturesCategory 期货分类
     * @return 结果
     */
    @Override
    public int insertStFuturesCategory(StFuturesCategory stFuturesCategory)
    {
        stFuturesCategory.setCreateTime(DateUtils.getNowDate());
        return stFuturesCategoryMapper.insertStFuturesCategory(stFuturesCategory);
    }

    /**
     * 修改期货分类
     * 
     * @param stFuturesCategory 期货分类
     * @return 结果
     */
    @Override
    public int updateStFuturesCategory(StFuturesCategory stFuturesCategory)
    {
        stFuturesCategory.setUpdateTime(DateUtils.getNowDate());
        return stFuturesCategoryMapper.updateStFuturesCategory(stFuturesCategory);
    }

    /**
     * 删除期货分类对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStFuturesCategoryByIds(String ids)
    {
        return stFuturesCategoryMapper.deleteStFuturesCategoryByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除期货分类信息
     * 
     * @param id 期货分类ID
     * @return 结果
     */
    public int deleteStFuturesCategoryById(Long id)
    {
        return stFuturesCategoryMapper.deleteStFuturesCategoryById(id);
    }

    /**
     * 查询期货分类树列表
     * 
     * @return 所有期货分类信息
     */
    @Override
    public List<Ztree> selectStFuturesCategoryTree()
    {
        List<StFuturesCategory> stFuturesCategoryList = stFuturesCategoryMapper.selectStFuturesCategoryList(new StFuturesCategory());
        List<Ztree> ztrees = new ArrayList<Ztree>();
        for (StFuturesCategory stFuturesCategory : stFuturesCategoryList)
        {
            Ztree ztree = new Ztree();
            ztree.setId(stFuturesCategory.getId());
            ztree.setpId(stFuturesCategory.getParentId());
            ztree.setName(stFuturesCategory.getNodeName());
            ztree.setTitle(stFuturesCategory.getNodeName());
            ztrees.add(ztree);
        }
        return ztrees;
    }
}
