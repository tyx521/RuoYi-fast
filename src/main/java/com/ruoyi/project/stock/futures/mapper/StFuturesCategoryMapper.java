package com.ruoyi.project.stock.futures.mapper;


import com.ruoyi.project.stock.futures.domain.StFuturesCategory;

import java.util.List;

/**
 * 期货分类Mapper接口
 * 
 * @author ruoyi
 * @date 2019-08-20
 */
public interface StFuturesCategoryMapper 
{
    /**
     * 查询期货分类
     * 
     * @param id 期货分类ID
     * @return 期货分类
     */
    public StFuturesCategory selectStFuturesCategoryById(Long id);

    /**
     * 查询期货分类列表
     * 
     * @param stFuturesCategory 期货分类
     * @return 期货分类集合
     */
    public List<StFuturesCategory> selectStFuturesCategoryList(StFuturesCategory stFuturesCategory);

    /**
     * 新增期货分类
     * 
     * @param stFuturesCategory 期货分类
     * @return 结果
     */
    public int insertStFuturesCategory(StFuturesCategory stFuturesCategory);

    /**
     * 修改期货分类
     * 
     * @param stFuturesCategory 期货分类
     * @return 结果
     */
    public int updateStFuturesCategory(StFuturesCategory stFuturesCategory);

    /**
     * 删除期货分类
     * 
     * @param id 期货分类ID
     * @return 结果
     */
    public int deleteStFuturesCategoryById(Long id);

    /**
     * 批量删除期货分类
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStFuturesCategoryByIds(String[] ids);
}
