package com.ruoyi.project.stock.futures.service;


import com.ruoyi.project.stock.futures.domain.StFuturesDetail;
import java.util.List;

/**
 * 期货详情Service接口
 * 
 * @author tingyu
 * @date 2019-08-21
 */
public interface IStFuturesDetailService 
{
    /**
     * 查询期货详情
     * 
     * @param id 期货详情ID
     * @return 期货详情
     */
    public StFuturesDetail selectStFuturesDetailById(Long id);

    /**
     * 查询期货详情列表
     * 
     * @param stFuturesDetail 期货详情
     * @return 期货详情集合
     */
    public List<StFuturesDetail> selectStFuturesDetailList(StFuturesDetail stFuturesDetail);

    /**
     * 新增期货详情
     * 
     * @param stFuturesDetail 期货详情
     * @return 结果
     */
    public int insertStFuturesDetail(StFuturesDetail stFuturesDetail);

    /**
     * 修改期货详情
     * 
     * @param stFuturesDetail 期货详情
     * @return 结果
     */
    public int updateStFuturesDetail(StFuturesDetail stFuturesDetail);

    /**
     * 批量删除期货详情
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStFuturesDetailByIds(String ids);

    /**
     * 删除期货详情信息
     * 
     * @param id 期货详情ID
     * @return 结果
     */
    public int deleteStFuturesDetailById(Long id);
}
