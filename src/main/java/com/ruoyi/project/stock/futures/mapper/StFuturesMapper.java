package com.ruoyi.project.stock.futures.mapper;

import com.ruoyi.project.stock.futures.domain.StFutures;
import java.util.List;

/**
 * 期货Mapper接口
 * 
 * @author tingyu
 * @date 2019-08-21
 */
public interface StFuturesMapper 
{
    /**
     * 查询期货
     * 
     * @param id 期货ID
     * @return 期货
     */
    public StFutures selectStFuturesById(Long id);

    /**
     * 查询期货列表
     * 
     * @param stFutures 期货
     * @return 期货集合
     */
    public List<StFutures> selectStFuturesList(StFutures stFutures);

    /**
     * 新增期货
     * 
     * @param stFutures 期货
     * @return 结果
     */
    public int insertStFutures(StFutures stFutures);

    /**
     * 修改期货
     * 
     * @param stFutures 期货
     * @return 结果
     */
    public int updateStFutures(StFutures stFutures);

    /**
     * 删除期货
     * 
     * @param id 期货ID
     * @return 结果
     */
    public int deleteStFuturesById(Long id);

    /**
     * 批量删除期货
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStFuturesByIds(String[] ids);


    /**
     * 查询期货
     *
     * @param code 期货代码
     * @return 期货
     */
    public StFutures selectStFuturesByCode(String code);
}
