package com.ruoyi.project.stock.futures.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.stock.futures.mapper.StFuturesMapper;
import com.ruoyi.project.stock.futures.domain.StFutures;
import com.ruoyi.project.stock.futures.service.IStFuturesService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 期货Service业务层处理
 * 
 * @author tingyu
 * @date 2019-08-21
 */
@Service
public class StFuturesServiceImpl implements IStFuturesService 
{
    @Autowired
    private StFuturesMapper stFuturesMapper;

    /**
     * 查询期货
     * 
     * @param id 期货ID
     * @return 期货
     */
    @Override
    public StFutures selectStFuturesById(Long id)
    {
        return stFuturesMapper.selectStFuturesById(id);
    }

    /**
     * 查询期货列表
     * 
     * @param stFutures 期货
     * @return 期货
     */
    @Override
    public List<StFutures> selectStFuturesList(StFutures stFutures)
    {
        return stFuturesMapper.selectStFuturesList(stFutures);
    }

    /**
     * 新增期货
     * 
     * @param stFutures 期货
     * @return 结果
     */
    @Override
    public int insertStFutures(StFutures stFutures)
    {
        stFutures.setCreateTime(DateUtils.getNowDate());
        return stFuturesMapper.insertStFutures(stFutures);
    }

    /**
     * 修改期货
     * 
     * @param stFutures 期货
     * @return 结果
     */
    @Override
    public int updateStFutures(StFutures stFutures)
    {
        stFutures.setUpdateTime(DateUtils.getNowDate());
        return stFuturesMapper.updateStFutures(stFutures);
    }

    /**
     * 删除期货对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStFuturesByIds(String ids)
    {
        return stFuturesMapper.deleteStFuturesByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除期货信息
     * 
     * @param id 期货ID
     * @return 结果
     */
    public int deleteStFuturesById(Long id)
    {
        return stFuturesMapper.deleteStFuturesById(id);
    }


    public StFutures selectStFuturesByCode(String code){
        return stFuturesMapper.selectStFuturesByCode(code);

    }
}
