package com.ruoyi.project.stock.futures.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 期货对象 st_futures
 * 
 * @author tingyu
 * @date 2019-08-21
 */
public class StFutures extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 期货id */
    private Long id;

    /** 代码 */
    @Excel(name = "代码")
    private String futureCode;

    /** 名称 */
    @Excel(name = "名称")
    private String futureName;

    /** 类型 */
    @Excel(name = "类型")
    private String futureType;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFutureCode(String futureCode) 
    {
        this.futureCode = futureCode;
    }

    public String getFutureCode() 
    {
        return futureCode;
    }
    public void setFutureName(String futureName) 
    {
        this.futureName = futureName;
    }

    public String getFutureName() 
    {
        return futureName;
    }
    public void setFutureType(String futureType) 
    {
        this.futureType = futureType;
    }

    public String getFutureType() 
    {
        return futureType;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("futureCode", getFutureCode())
            .append("futureName", getFutureName())
            .append("futureType", getFutureType())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
