package com.ruoyi.project.stock.futures.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 期货详情对象 st_futures_detail
 * 
 * @author tingyu
 * @date 2019-08-21
 */
public class StFuturesDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键id */
    private Long id;

    /** 期货id */
    @Excel(name = "期货id")
    private Long futureId;

    /** 日期 */
    @Excel(name = "日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date dataTime;

    /** 现价 */
    @Excel(name = "现价")
    private Double nowPrice;

    /** 买价 */
    @Excel(name = "买价")
    private Double buyPrice;

    /** 卖价 */
    @Excel(name = "卖价")
    private Double sellPrice;

    /** 买量 */
    @Excel(name = "买量")
    private Long buyVolume;

    /** 卖量 */
    @Excel(name = "卖量")
    private Long sellVolume;

    /** 最高价 */
    @Excel(name = "最高价")
    private Double highestPrice;

    /** 最低价 */
    @Excel(name = "最低价")
    private Double lowestPrice;

    /** 开盘 */
    @Excel(name = "开盘")
    private Double openPrice;

    /** 昨收 */
    @Excel(name = "昨收")
    private Double prevClose;

    /** 昨结 */
    @Excel(name = "昨结")
    private Double prevSettle;

    /** 今结 */
    @Excel(name = "今结")
    private Double todaySettle;

    /** 涨跌 */
    @Excel(name = "涨跌")
    private Double upDownVal;

    /** 涨幅% */
    @Excel(name = "涨幅%")
    private Double upPercent;

    /** 现手 */
    @Excel(name = "现手")
    private Long nowHands;

    /** 总手 */
    @Excel(name = "总手")
    private Long totalHands;

    /** 持仓 */
    @Excel(name = "持仓")
    private Long holdPosition;

    /** 外盘 */
    @Excel(name = "外盘")
    private Long buyAmount;

    /** 内盘 */
    @Excel(name = "内盘")
    private Long sellAmount;

    /** 振幅 */
    @Excel(name = "振幅")
    private Double amplitude;

    /** 日增仓 */
    @Excel(name = "日增仓")
    private Long positions;

    /** 总金额 */
    @Excel(name = "总金额")
    private Long totalAmount;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setFutureId(Long futureId) 
    {
        this.futureId = futureId;
    }

    public Long getFutureId() 
    {
        return futureId;
    }
    public void setDataTime(Date dataTime) 
    {
        this.dataTime = dataTime;
    }

    public Date getDataTime() 
    {
        return dataTime;
    }
    public void setNowPrice(Double nowPrice) 
    {
        this.nowPrice = nowPrice;
    }

    public Double getNowPrice() 
    {
        return nowPrice;
    }
    public void setBuyPrice(Double buyPrice) 
    {
        this.buyPrice = buyPrice;
    }

    public Double getBuyPrice() 
    {
        return buyPrice;
    }
    public void setSellPrice(Double sellPrice) 
    {
        this.sellPrice = sellPrice;
    }

    public Double getSellPrice() 
    {
        return sellPrice;
    }
    public void setBuyVolume(Long buyVolume) 
    {
        this.buyVolume = buyVolume;
    }

    public Long getBuyVolume() 
    {
        return buyVolume;
    }
    public void setSellVolume(Long sellVolume) 
    {
        this.sellVolume = sellVolume;
    }

    public Long getSellVolume() 
    {
        return sellVolume;
    }
    public void setHighestPrice(Double highestPrice) 
    {
        this.highestPrice = highestPrice;
    }

    public Double getHighestPrice() 
    {
        return highestPrice;
    }
    public void setLowestPrice(Double lowestPrice) 
    {
        this.lowestPrice = lowestPrice;
    }

    public Double getLowestPrice() 
    {
        return lowestPrice;
    }
    public void setOpenPrice(Double openPrice) 
    {
        this.openPrice = openPrice;
    }

    public Double getOpenPrice() 
    {
        return openPrice;
    }
    public void setPrevClose(Double prevClose) 
    {
        this.prevClose = prevClose;
    }

    public Double getPrevClose() 
    {
        return prevClose;
    }
    public void setPrevSettle(Double prevSettle) 
    {
        this.prevSettle = prevSettle;
    }

    public Double getPrevSettle() 
    {
        return prevSettle;
    }
    public void setTodaySettle(Double todaySettle) 
    {
        this.todaySettle = todaySettle;
    }

    public Double getTodaySettle() 
    {
        return todaySettle;
    }
    public void setUpDownVal(Double upDownVal) 
    {
        this.upDownVal = upDownVal;
    }

    public Double getUpDownVal() 
    {
        return upDownVal;
    }
    public void setUpPercent(Double upPercent) 
    {
        this.upPercent = upPercent;
    }

    public Double getUpPercent() 
    {
        return upPercent;
    }
    public void setNowHands(Long nowHands) 
    {
        this.nowHands = nowHands;
    }

    public Long getNowHands() 
    {
        return nowHands;
    }
    public void setTotalHands(Long totalHands) 
    {
        this.totalHands = totalHands;
    }

    public Long getTotalHands() 
    {
        return totalHands;
    }
    public void setHoldPosition(Long holdPosition) 
    {
        this.holdPosition = holdPosition;
    }

    public Long getHoldPosition() 
    {
        return holdPosition;
    }
    public void setBuyAmount(Long buyAmount) 
    {
        this.buyAmount = buyAmount;
    }

    public Long getBuyAmount() 
    {
        return buyAmount;
    }
    public void setSellAmount(Long sellAmount) 
    {
        this.sellAmount = sellAmount;
    }

    public Long getSellAmount() 
    {
        return sellAmount;
    }
    public void setAmplitude(Double amplitude) 
    {
        this.amplitude = amplitude;
    }

    public Double getAmplitude() 
    {
        return amplitude;
    }
    public void setPositions(Long positions) 
    {
        this.positions = positions;
    }

    public Long getPositions() 
    {
        return positions;
    }
    public void setTotalAmount(Long totalAmount) 
    {
        this.totalAmount = totalAmount;
    }

    public Long getTotalAmount() 
    {
        return totalAmount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("futureId", getFutureId())
            .append("dataTime", getDataTime())
            .append("nowPrice", getNowPrice())
            .append("buyPrice", getBuyPrice())
            .append("sellPrice", getSellPrice())
            .append("buyVolume", getBuyVolume())
            .append("sellVolume", getSellVolume())
            .append("highestPrice", getHighestPrice())
            .append("lowestPrice", getLowestPrice())
            .append("openPrice", getOpenPrice())
            .append("prevClose", getPrevClose())
            .append("prevSettle", getPrevSettle())
            .append("todaySettle", getTodaySettle())
            .append("upDownVal", getUpDownVal())
            .append("upPercent", getUpPercent())
            .append("nowHands", getNowHands())
            .append("totalHands", getTotalHands())
            .append("holdPosition", getHoldPosition())
            .append("buyAmount", getBuyAmount())
            .append("sellAmount", getSellAmount())
            .append("amplitude", getAmplitude())
            .append("positions", getPositions())
            .append("totalAmount", getTotalAmount())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
