package com.ruoyi.project.stock.futures.controller;

import java.util.List;

import com.ruoyi.project.stock.futures.domain.StFuturesDetail;
import com.ruoyi.project.stock.futures.service.IStFuturesDetailService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 期货详情Controller
 * 
 * @author tingyu
 * @date 2019-08-21
 */
@Controller
@RequestMapping("/stock/futuresdetail")
public class StFuturesDetailController extends BaseController
{
    private String prefix = "stock/futures/futuresdetail";

    @Autowired
    private IStFuturesDetailService stFuturesDetailService;

    @RequiresPermissions("stock:futuresdetail:view")
    @GetMapping()
    public String futuresdetail()
    {
        return prefix + "/futuresdetail";
    }

    /**
     * 查询期货详情列表
     */
    @RequiresPermissions("stock:futuresdetail:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StFuturesDetail stFuturesDetail)
    {
        startPage();
        List<StFuturesDetail> list = stFuturesDetailService.selectStFuturesDetailList(stFuturesDetail);
        return getDataTable(list);
    }

    /**
     * 导出期货详情列表
     */
    @RequiresPermissions("stock:futuresdetail:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StFuturesDetail stFuturesDetail)
    {
        List<StFuturesDetail> list = stFuturesDetailService.selectStFuturesDetailList(stFuturesDetail);
        ExcelUtil<StFuturesDetail> util = new ExcelUtil<StFuturesDetail>(StFuturesDetail.class);
        return util.exportExcel(list, "futuresdetail");
    }

    /**
     * 新增期货详情
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存期货详情
     */
    @RequiresPermissions("stock:futuresdetail:add")
    @Log(title = "期货详情", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StFuturesDetail stFuturesDetail)
    {
        return toAjax(stFuturesDetailService.insertStFuturesDetail(stFuturesDetail));
    }

    /**
     * 修改期货详情
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StFuturesDetail stFuturesDetail = stFuturesDetailService.selectStFuturesDetailById(id);
        mmap.put("stFuturesDetail", stFuturesDetail);
        return prefix + "/edit";
    }

    /**
     * 修改保存期货详情
     */
    @RequiresPermissions("stock:futuresdetail:edit")
    @Log(title = "期货详情", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StFuturesDetail stFuturesDetail)
    {
        return toAjax(stFuturesDetailService.updateStFuturesDetail(stFuturesDetail));
    }

    /**
     * 删除期货详情
     */
    @RequiresPermissions("stock:futuresdetail:remove")
    @Log(title = "期货详情", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stFuturesDetailService.deleteStFuturesDetailByIds(ids));
    }
}
