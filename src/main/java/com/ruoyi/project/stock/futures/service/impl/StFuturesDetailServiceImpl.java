package com.ruoyi.project.stock.futures.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.project.stock.futures.domain.StFuturesDetail;
import com.ruoyi.project.stock.futures.mapper.StFuturesDetailMapper;
import com.ruoyi.project.stock.futures.service.IStFuturesDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.common.utils.text.Convert;

/**
 * 期货详情Service业务层处理
 * 
 * @author tingyu
 * @date 2019-08-21
 */
@Service
public class StFuturesDetailServiceImpl implements IStFuturesDetailService
{
    @Autowired
    private StFuturesDetailMapper stFuturesDetailMapper;

    /**
     * 查询期货详情
     * 
     * @param id 期货详情ID
     * @return 期货详情
     */
    @Override
    public StFuturesDetail selectStFuturesDetailById(Long id)
    {
        return stFuturesDetailMapper.selectStFuturesDetailById(id);
    }

    /**
     * 查询期货详情列表
     * 
     * @param stFuturesDetail 期货详情
     * @return 期货详情
     */
    @Override
    public List<StFuturesDetail> selectStFuturesDetailList(StFuturesDetail stFuturesDetail)
    {
        return stFuturesDetailMapper.selectStFuturesDetailList(stFuturesDetail);
    }

    /**
     * 新增期货详情
     * 
     * @param stFuturesDetail 期货详情
     * @return 结果
     */
    @Override
    public int insertStFuturesDetail(StFuturesDetail stFuturesDetail)
    {
        stFuturesDetail.setCreateTime(DateUtils.getNowDate());
        return stFuturesDetailMapper.insertStFuturesDetail(stFuturesDetail);
    }

    /**
     * 修改期货详情
     * 
     * @param stFuturesDetail 期货详情
     * @return 结果
     */
    @Override
    public int updateStFuturesDetail(StFuturesDetail stFuturesDetail)
    {
        stFuturesDetail.setUpdateTime(DateUtils.getNowDate());
        return stFuturesDetailMapper.updateStFuturesDetail(stFuturesDetail);
    }

    /**
     * 删除期货详情对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStFuturesDetailByIds(String ids)
    {
        return stFuturesDetailMapper.deleteStFuturesDetailByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除期货详情信息
     * 
     * @param id 期货详情ID
     * @return 结果
     */
    public int deleteStFuturesDetailById(Long id)
    {
        return stFuturesDetailMapper.deleteStFuturesDetailById(id);
    }
}
