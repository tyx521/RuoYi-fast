package com.ruoyi.project.stock.futures.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.stock.futures.domain.StFutures;
import com.ruoyi.project.stock.futures.service.IStFuturesService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 期货Controller
 * 
 * @author tingyu
 * @date 2019-08-21
 */
@Controller
@RequestMapping("/stock/futures")
public class StFuturesController extends BaseController
{
    private String prefix = "stock/futures/futures";

    @Autowired
    private IStFuturesService stFuturesService;

    @RequiresPermissions("stock:futures:view")
    @GetMapping()
    public String futures()
    {
        return prefix + "/futures";
    }

    /**
     * 查询期货列表
     */
    @RequiresPermissions("stock:futures:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StFutures stFutures)
    {
        startPage();
        List<StFutures> list = stFuturesService.selectStFuturesList(stFutures);
        return getDataTable(list);
    }

    /**
     * 导出期货列表
     */
    @RequiresPermissions("stock:futures:export")
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StFutures stFutures)
    {
        List<StFutures> list = stFuturesService.selectStFuturesList(stFutures);
        ExcelUtil<StFutures> util = new ExcelUtil<StFutures>(StFutures.class);
        return util.exportExcel(list, "futures");
    }

    /**
     * 新增期货
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存期货
     */
    @RequiresPermissions("stock:futures:add")
    @Log(title = "期货", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StFutures stFutures)
    {
        return toAjax(stFuturesService.insertStFutures(stFutures));
    }

    /**
     * 修改期货
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StFutures stFutures = stFuturesService.selectStFuturesById(id);
        mmap.put("stFutures", stFutures);
        return prefix + "/edit";
    }

    /**
     * 修改保存期货
     */
    @RequiresPermissions("stock:futures:edit")
    @Log(title = "期货", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StFutures stFutures)
    {
        return toAjax(stFuturesService.updateStFutures(stFutures));
    }

    /**
     * 删除期货
     */
    @RequiresPermissions("stock:futures:remove")
    @Log(title = "期货", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(stFuturesService.deleteStFuturesByIds(ids));
    }
}
