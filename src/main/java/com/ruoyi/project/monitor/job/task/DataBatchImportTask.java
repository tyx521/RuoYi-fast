package com.ruoyi.project.monitor.job.task;


import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.NumberUtils;
import com.ruoyi.project.stock.bond.domain.StBond;
import com.ruoyi.project.stock.bond.domain.StBondDetail;
import com.ruoyi.project.stock.bond.service.IStBondDetailService;
import com.ruoyi.project.stock.bond.service.IStBondService;
import com.ruoyi.project.stock.exchangen.domain.StForeignExchangen;
import com.ruoyi.project.stock.exchangen.domain.StForeignExchangenDetail;
import com.ruoyi.project.stock.exchangen.service.IStForeignExchangenDetailService;
import com.ruoyi.project.stock.exchangen.service.IStForeignExchangenService;
import com.ruoyi.project.stock.foundation.domain.StFoundation;
import com.ruoyi.project.stock.foundation.domain.StFoundationDetail;
import com.ruoyi.project.stock.foundation.service.IStFoundationDetailService;
import com.ruoyi.project.stock.foundation.service.IStFoundationService;
import com.ruoyi.project.stock.futures.domain.StFutures;
import com.ruoyi.project.stock.futures.domain.StFuturesDetail;
import com.ruoyi.project.stock.futures.service.IStFuturesCategoryService;
import com.ruoyi.project.stock.futures.service.IStFuturesDetailService;
import com.ruoyi.project.stock.futures.service.IStFuturesService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;

/**
 * 用于导入证券的各种数据
 * @date: 2019-08-13
 * @author: tingyu
 */

@Component("dataBatchImportTask")
public class DataBatchImportTask {
    private static final Logger log = LoggerFactory.getLogger(DataBatchImportTask.class);
    //数据文件存放目录
    public static String DATA_FILE_PATH="C:\\Users\\Administrator\\Desktop\\同花顺数据导出\\20190820\\转换\\";
    //备份目录
    public static String BACK_FILE_PATH="";

    @Autowired
    private IStBondService stBondService;
    @Autowired
    private IStBondDetailService stBondDetailService;

    @Autowired
    private IStFoundationService foundationService;
    @Autowired
    private IStFoundationDetailService foundationDetailService;

    @Autowired
    private IStForeignExchangenService foreignExchangenService;
    @Autowired
    private IStForeignExchangenDetailService foreignExchangenDetailService;

    @Autowired
    private IStFuturesService futuresService;
    @Autowired
    private IStFuturesCategoryService futuresCategoryService;
    @Autowired
    private IStFuturesDetailService futuresDetailService;



    public static  final  String  IMPORT_TYPE_BOND="债券";
    public static  final  String  IMPORT_TYPE_EXCHANGE="外汇";
    public static  final  String  IMPORT_TYPE_FUTURES="期货";

    public static Map<String,String> bondMap = new HashMap<>();  //用于存放债券类型
    public static Map<String,String> foundationMap=new HashMap<>(); //用于存放基金的网址
    public static Map<String,String> exchangeMap = new HashMap<>(); //用户存放外汇类型
    public static Map<String,Long> futuresMap = new HashMap<>(); //存放期货分类


    static {
        //用于存放债券类型
        bondMap.put("地方债","1");
        bondMap.put("国债","2");
        bondMap.put("回购","3");
        bondMap.put("可转债","4");
        bondMap.put("企业债","5");
        bondMap.put("贴债","6");

        //基金类型
        foundationMap.put("1","http://fund.ijijin.cn/data/Net/info/gpx_F009_desc_0_0_1_9999_0_0_0_jsonp_g.html");   //股票型
        foundationMap.put("2","http://fund.ijijin.cn/data/Net/info/hhx_F009_desc_0_0_1_9999_0_0_0_jsonp_g.html");    //混合型
        foundationMap.put("3","http://fund.ijijin.cn/data/Net/info/zqx_F009_desc_0_0_1_9999_0_0_0_jsonp_g.html");    //债券型
        foundationMap.put("4","http://fund.ijijin.cn/data/Net/hbx/hbx_F009_desc_0_0_1_9999_0_0_0_jsonp_g.html");     //货币型
        foundationMap.put("5","http://fund.ijijin.cn/data/Net/hbx/dqlc_F009_desc_0_0_1_9999_0_0_0_jsonp_g.html");    //短期理财
        foundationMap.put("6","http://fund.ijijin.cn/data/Net/info/QDII_F009_desc_0_0_1_9999_0_0_0_jsonp_g.html");   //QDII
        foundationMap.put("7","http://fund.ijijin.cn/data/Net/info/bbx_F009_desc_0_0_1_9999_0_0_0_jsonp_g.html");    //保本型
        foundationMap.put("8","http://fund.ijijin.cn/data/Net/info/zsx_F009_desc_0_0_1_9999_0_0_0_jsonp_g.html");    //指数型

        //外汇
        exchangeMap.put("基本汇率","1");
        exchangeMap.put("反向汇率","2");
        exchangeMap.put("交叉汇率","3");

        futuresMap.put("伦敦ICE",16L);
        futuresMap.put("马来西亚BMD",18L);
        futuresMap.put("纽约COMEX",13L);
        futuresMap.put("纽约ICE",15L);
        futuresMap.put("纽约NYMEX",12L);
        futuresMap.put("外盘期货主力",11L);
        futuresMap.put("新加坡SGX",17L);
        futuresMap.put("芝加哥CBOT",14L);
        futuresMap.put("股指期货",9L);
        futuresMap.put("国债期货",10L);

    }

    /**
     * 导入债券数据
     */
    public void importBond(){
        Date currentDate = new Date();
        List<File> fileList=getFilesByName(IMPORT_TYPE_BOND);

        if(fileList !=null && fileList.size() > 0){
            for(File f : fileList){
                Sheet sheet = getSheet(f);
                if(sheet !=null){
                    String[]  fileNameSplit= f.getName().split("\\.")[0].split("_");
                    String busiType = fileNameSplit[1];    //分类
                    String busiDate = fileNameSplit[2];    //业务时间
                    String busiDateFormat = busiDate.substring(0,4) + "-" + busiDate.substring(4,6) + "-" + busiDate.substring(6,8);
                    if(bondMap.get(busiType) !=null && !"".equals(bondMap.get(busiType))){
                        Row row0 = sheet.getRow(0);
                        Row row = null;
                        for(int i=1;i<sheet.getLastRowNum();i++){
                            row = sheet.getRow(i);
                            if(row !=null){
                                String code=getCellVal(row.getCell(0))==null?"":getCellVal(row.getCell(0)).toString();   //代码
                                String name=getCellVal(row.getCell(1))==null?"":getCellVal(row.getCell(1)).toString();  //名称
                                String interest=getCellVal(row.getCell(3))==null?"":getCellVal(row.getCell(3)).toString();   //利率
                                String rates=getCellVal(row.getCell(4))==null?"":getCellVal(row.getCell(4)).toString();   //利息
                                String upPercent=getCellVal(row.getCell(5))==null?"":getCellVal(row.getCell(5)).toString();   //涨幅%
                                String nowPrice=getCellVal(row.getCell(6))==null?"":getCellVal(row.getCell(6)).toString();   //现价
                                String totalHands=getCellVal(row.getCell(7))==null?"":getCellVal(row.getCell(7)).toString();   //总手
                                String prevClose=getCellVal(row.getCell(8))==null?"":getCellVal(row.getCell(8)).toString();   //昨收
                                String openPrice=getCellVal(row.getCell(9))==null?"":getCellVal(row.getCell(9)).toString();   //开盘
                                String highestPrice=getCellVal(row.getCell(10))==null?"":getCellVal(row.getCell(10)).toString();   //最高
                                String lowestPrice=getCellVal(row.getCell(11))==null?"":getCellVal(row.getCell(11)).toString();   //最低
                                String buyPrice=getCellVal(row.getCell(12))==null?"":getCellVal(row.getCell(12)).toString();   //买价
                                String sellPrice=getCellVal(row.getCell(13))==null?"":getCellVal(row.getCell(13)).toString();   //卖价
                                String totalAmount=getCellVal(row.getCell(14))==null?"":getCellVal(row.getCell(14)).toString();   //总金额
                                String buyVolume=getCellVal(row.getCell(15))==null?"":getCellVal(row.getCell(15)).toString();   //买量
                                String sellVolume=getCellVal(row.getCell(16))==null?"":getCellVal(row.getCell(16)).toString();   //卖量

                                //数据处理
                                if(code !=null && !code.equals("")){
                                    code = code.replaceAll("SZ","").replaceAll("SH","");

                                    //先判断该数据是否存在
                                    StBond bond = stBondService.selectStBondByCode(code);
                                    if(bond == null){
                                        //新增 st_bond表
                                        bond = new StBond();
                                        bond.setBondCode(code);
                                        bond.setBondName(name.trim());
                                        bond.setBondType(bondMap.get(busiType));
                                        bond.setCreateBy("Admin");
                                        bond.setCreateTime(currentDate);
                                        int res = stBondService.insertStBond(bond);
                                    }
                                    //判断st_bond_detail表中当前的记录是否存在，不存在则新增
                                    StBondDetail detail = new StBondDetail();
                                    detail.setBondId(bond.getBondId());
                                    detail.setDataTime(DateUtils.parseDate(busiDateFormat));
                                    List<StBondDetail> detailList=stBondDetailService.selectStBondDetailList(detail);
                                    if(detailList==null || detailList.size()==0){
                                        //说明记录不存在
                                        if(NumberUtils.isNum(interest)){
                                            detail.setInterest(Double.parseDouble(interest));
                                        }
                                        if(NumberUtils.isNum(rates)){
                                            detail.setRates(Double.parseDouble(rates));
                                        }
                                        if(NumberUtils.isNum(upPercent)){
                                            detail.setUpPercent(Double.parseDouble(upPercent));
                                        }
                                        if(NumberUtils.isNum(nowPrice)){
                                            detail.setNowPrice(Double.parseDouble(nowPrice));
                                        }
                                        if(NumberUtils.isNum(totalHands)){
                                            detail.setTotalHands((new Double(totalHands)).longValue());
                                        }
                                        if(NumberUtils.isNum(prevClose)){
                                            detail.setPrevClose(Double.parseDouble(prevClose));
                                        }
                                        if(NumberUtils.isNum(openPrice)){
                                            detail.setOpenPrice(Double.parseDouble(openPrice));
                                        }
                                        if(NumberUtils.isNum(highestPrice)){
                                            detail.setHighestPrice(Double.parseDouble(highestPrice));
                                        }
                                        if(NumberUtils.isNum(lowestPrice)){
                                            detail.setLowestPrice(Double.parseDouble(lowestPrice));
                                        }
                                        if(NumberUtils.isNum(buyPrice)){
                                            detail.setBuyPrice(Double.parseDouble(buyPrice));
                                        }
                                        if(NumberUtils.isNum(sellPrice)){
                                            detail.setSellPrice(Double.parseDouble(sellPrice));
                                        }
                                        if(NumberUtils.isNum(totalAmount)){
                                            detail.setTotalAmount((new Double(totalAmount)).longValue());
                                        }
                                        if(NumberUtils.isNum(buyVolume)){
                                            detail.setBuyVolume((new Double(buyVolume)).longValue());
                                        }
                                        if(NumberUtils.isNum(sellVolume)){
                                            detail.setSellVolume((new Double(sellVolume)).longValue());
                                        }
                                        detail.setCreateBy("Admin");
                                        detail.setCreateTime(currentDate);
                                        stBondDetailService.insertStBondDetail(detail);
                                    }
                                }
                            }
                        }
                    }else{
                        log.error("类型为"+ busiType + " 债券信息不存在，请检查导入的数据文件是否正确.");
                    }
                }
            }
        }else {
            log.info("没有找到对应的债券导入数据....");
        }
    }


    /**
     * 通过网络下载基金数据
     */
    public void downlaodFoundationDataByNet(){
        try {
            Date currentDate =new Date();
            for (String key:foundationMap.keySet()){
                String url=foundationMap.get(key);
                Connection connect =Jsoup.connect(url);
                connect.method(Connection.Method.GET);
                connect.maxBodySize(0);
                connect.timeout(24000000);
                connect.ignoreHttpErrors(true);
                connect.ignoreContentType(true);
                Document doc= connect.get();
                //Connection.Response response = connect.execute();
                if(doc !=null){
                    String data = doc.body().html();
                    log.info("界面数据："+data);
                    if(data !=null && data.startsWith("g")){
                        data= data.substring(2,data.length()-1);
                    }
                    //JSONObject json = new JSONObject(data);
                    JSONObject json =JSONObject.parseObject(data);
                    //System.out.println(json);
                    if(json !=null){
                        if(json.get("data") !=null){
                            json=json.getJSONObject("data");
                            if(json.get("data") !=null){
                                json=json.getJSONObject("data");
                                if(json !=null){
                                    Set<String> set=json.keySet();
                                    for (String codeKey:set){
                                        JSONObject foundObj=json.getJSONObject(codeKey);
                                        log.info("当前记录：" + foundObj.toJSONString());
                                        if(foundObj !=null){
                                            StFoundation found = null;
                                            String code = foundObj.getString("code");
                                            //判断当前的记录是否存在
                                            found = foundationService.selectStFoundationByCode(code);
                                            if(found==null){
                                                found = new StFoundation();
                                                found.setFoundCode(code);                                //代码
                                                found.setFoundName(foundObj.getString("name"));   //名称
                                                found.setOrganName(foundObj.getString("orgname"));//所属组织机构
                                                found.setBondType(key);                                  //类型
                                                found.setCreateBy("Admin");
                                                found.setCreateTime(currentDate);
                                                foundationService.insertStFoundation(found);            //插入数据
                                            }
                                            if(found !=null && found.getId() > 0){
                                                StFoundationDetail detail = new StFoundationDetail();
                                                detail.setFoundId(found.getId());
                                                if(foundObj.get("newdate") !=null && !"".equals(foundObj.getString("newdate"))){
                                                    //如果日期为，则不插入
                                                    detail.setDataTime(DateUtils.parseDate(foundObj.getString("newdate")));   //当前日期
                                                    List<StFoundationDetail> list=foundationDetailService.selectStFoundationDetailList(detail);
                                                    if(list == null || list.size()==0){
                                                        //说明没有数据，进行插入操作
                                                        if(foundObj.get("newnet") !=null && !"".equals(foundObj.getString("newnet"))){
                                                            detail.setNetValue(Double.parseDouble(foundObj.getString("newnet")));           //净值
                                                        }

                                                        if(foundObj.get("totalnet") !=null && !"".equals(foundObj.getString("totalnet"))){
                                                            detail.setTotalNetValue(Double.parseDouble(foundObj.getString("totalnet")));    //累计净值
                                                        }

                                                        if(foundObj.get("rate") !=null && !"".equals(foundObj.getString("rate"))){
                                                            detail.setDayPercent(Double.parseDouble(foundObj.getString("rate")));           //日增长率
                                                        }

                                                        if(foundObj.get("F003N_FUND33") !=null && !"".equals(foundObj.getString("F003N_FUND33"))){
                                                            detail.setWeekPercent(Double.parseDouble(foundObj.getString("F003N_FUND33")));  //周增长率
                                                        }

                                                        if(foundObj.get("F008") !=null && !"".equals(foundObj.getString("F008"))){
                                                            detail.setMonthPercent(Double.parseDouble(foundObj.getString("F008")));         //月增长率
                                                        }

                                                        if(foundObj.get("F009") !=null && !"".equals(foundObj.getString("F009"))){
                                                            detail.setThreeMonthPercent(Double.parseDouble(foundObj.getString("F009")));    //近三个月增长率
                                                        }

                                                        if(foundObj.get("F011") !=null && !"".equals(foundObj.getString("F011"))){
                                                            detail.setYearPercent(Double.parseDouble(foundObj.getString("F011")));          //近一年增长率
                                                        }

                                                        if(foundObj.get("F012") !=null && !"".equals(foundObj.getString("F012"))){
                                                            detail.setYearPercent(Double.parseDouble(foundObj.getString("F012")));          //成立以来增长率
                                                        }
                                                        foundationDetailService.insertStFoundationDetail(detail);
                                                    }else{
                                                        log.info("代码"+codeKey+"的基金数据已导入.");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("导入基金数据出错\n"+e.getLocalizedMessage());
            e.printStackTrace();
        }
    }


    /**
     *导入外汇数据
     */
    public void importExchange(){
        log.info("开始导入外汇数据信息.");
        Date currentDate = new Date();
        List<File> fileList=getFilesByName(IMPORT_TYPE_EXCHANGE);
        if(fileList !=null && fileList.size() > 0){
            for(File f : fileList){
                Sheet sheet = getSheet(f);
                if(sheet !=null){
                    String[]  fileNameSplit= f.getName().split("\\.")[0].split("_");
                    String busiType = fileNameSplit[1];    //分类
                    String busiDate = fileNameSplit[2];    //业务时间
                    String busiDateFormat = busiDate.substring(0,4) + "-" + busiDate.substring(4,6) + "-" + busiDate.substring(6,8);
                    if(exchangeMap.get(busiType) !=null && !"".equals(bondMap.get(busiType))){
                        Row row0 = sheet.getRow(0);
                        Row row = null;
                        for(int i=1;i<sheet.getLastRowNum();i++){
                            row = sheet.getRow(i);
                            if(row !=null){
                                String code=getCellVal(row.getCell(0))==null?"":getCellVal(row.getCell(0)).toString();   //代码
                                String name=getCellVal(row.getCell(1))==null?"":getCellVal(row.getCell(1)).toString();  //名称
                                String nowPrice=getCellVal(row.getCell(3))==null?"":getCellVal(row.getCell(3)).toString();   //现价
                                String upDownVal=getCellVal(row.getCell(4))==null?"":getCellVal(row.getCell(4)).toString();   //涨跌
                                String buyPrice=getCellVal(row.getCell(5))==null?"":getCellVal(row.getCell(5)).toString();   //买价
                                String sellPrice=getCellVal(row.getCell(6))==null?"":getCellVal(row.getCell(6)).toString();   //卖价
                                String amplitude=getCellVal(row.getCell(7))==null?"":getCellVal(row.getCell(7)).toString();   //振幅
                                String upPercent=getCellVal(row.getCell(8))==null?"":getCellVal(row.getCell(8)).toString();   //涨幅
                                String upPercentOne=getCellVal(row.getCell(9))==null?"":getCellVal(row.getCell(9)).toString();   //1分钟涨幅
                                String upPercentThree=getCellVal(row.getCell(10))==null?"":getCellVal(row.getCell(10)).toString();   //3分钟涨幅
                                String highestPrice=getCellVal(row.getCell(11))==null?"":getCellVal(row.getCell(11)).toString();   //最高
                                String lowestPrice=getCellVal(row.getCell(12))==null?"":getCellVal(row.getCell(12)).toString();   //最低
                                String openPrice=getCellVal(row.getCell(13))==null?"":getCellVal(row.getCell(13)).toString();   //开盘
                                String runout=getCellVal(row.getCell(14))==null?"":getCellVal(row.getCell(14)).toString();   //跳动数

                                //数据处理
                                if(code !=null && !code.equals("")){
                                    //先判断该数据是否存在
                                    StForeignExchangen exchangen = foreignExchangenService.selectStForeignExchangenByCode(code);
                                    if(exchangen == null){
                                        //新增 st_foreign_exchange表
                                        exchangen = new StForeignExchangen();
                                        exchangen.setExchangenCode(code);
                                        exchangen.setExchangenName(name);
                                        exchangen.setExchangenType(exchangeMap.get(busiType));
                                        exchangen.setCreateBy("Admin");
                                        exchangen.setCreateTime(currentDate);
                                        int res = foreignExchangenService.insertStForeignExchangen(exchangen);
                                    }
                                    //判断st_foreign_exchange表_detail表中当前的记录是否存在，不存在则新增
                                    StForeignExchangenDetail detail = new StForeignExchangenDetail();
                                    detail.setExchangenId(exchangen.getId());
                                    detail.setDataTime(DateUtils.parseDate(busiDateFormat));
                                    List<StForeignExchangenDetail> detailList=foreignExchangenDetailService.selectStForeignExchangenDetailList(detail);
                                    if(detailList==null || detailList.size()==0){
                                        //说明记录不存在
                                        if(NumberUtils.isNum(nowPrice)){
                                            detail.setNowPrice(Double.parseDouble(nowPrice));   //现价
                                        }
                                        if(NumberUtils.isNum(upDownVal)){
                                            detail.setUpDownVal(Double.parseDouble(upDownVal));   //涨跌
                                        }
                                        if(NumberUtils.isNum(buyPrice)){
                                            detail.setBuyPrice(Double.parseDouble(buyPrice));   //买价
                                        }
                                        if(NumberUtils.isNum(sellPrice)){
                                            detail.setSellPrice(Double.parseDouble(sellPrice));   //卖价
                                        }
                                        //振幅
                                        if(amplitude !=null && !"".equals(amplitude)){
                                            if(amplitude.endsWith("%")){
                                                amplitude = amplitude.substring(0,amplitude.length()-1);  //去掉%
                                                if(!amplitude.equals("0.00")){
                                                    detail.setAmplitude(Double.parseDouble(amplitude)/100);    //转换为小数
                                                }
                                            }
                                        }
                                        //涨幅
                                        if(upPercent !=null && !"".equals(upPercent) && !upPercent.contains("--")){
                                            if(upPercent.endsWith("%")){
                                                upPercent = upPercent.substring(0,upPercent.length()-1);  //去掉%
                                                if(!upPercent.equals("0.00")){
                                                    detail.setUpPercent(Double.parseDouble(upPercent)/100);    //转换为小数
                                                }
                                            }else{
                                                detail.setUpPercent(Double.parseDouble(upPercent));
                                            }
                                        }

                                        //1天涨幅
                                        if(upPercentOne !=null && !"".equals(upPercentOne) && !upPercentOne.contains("--")){
                                            if(upPercentOne.endsWith("%")){
                                                upPercentOne = upPercentOne.substring(0,upPercentOne.length()-1);  //去掉%
                                                if(!upPercentOne.equals("0.00")){
                                                    detail.setUpPercentOne(Double.parseDouble(upPercentOne)/100);    //转换为小数
                                                }
                                            }else{
                                                detail.setUpPercentOne(Double.parseDouble(upPercentOne));
                                            }
                                        }

                                        //3天涨幅
                                        if(upPercentThree !=null && !"".equals(upPercentThree) && !upPercentThree.contains("--")){
                                            if(upPercentThree.endsWith("%")){
                                                upPercentThree = upPercentThree.substring(0,upPercentThree.length()-1);  //去掉%
                                                if(!upPercentThree.equals("0.00")){
                                                    detail.setUpPercentThree(Double.parseDouble(upPercentThree)/100);    //转换为小数
                                                }
                                            }else{
                                                detail.setUpPercentThree(Double.parseDouble(upPercentThree));
                                            }
                                        }

                                        if(NumberUtils.isNum(highestPrice)){
                                            detail.setHighestPrice(Double.parseDouble(highestPrice));     //最高
                                        }
                                        if(NumberUtils.isNum(lowestPrice)){
                                            detail.setLowestPrice(Double.parseDouble(lowestPrice));       //最低
                                        }
                                        if(NumberUtils.isNum(openPrice)){
                                            detail.setOpenPrice(Double.parseDouble(openPrice));           //开盘价
                                        }
                                        if(NumberUtils.isNum(runout)){
                                            if(runout.contains(".")){
                                                detail.setRunout((new Double(runout)).longValue());
                                            }else{
                                                detail.setRunout(Long.parseLong(runout));                     //跳动度
                                            }
                                        }
                                        detail.setCreateBy("Admin");
                                        detail.setCreateTime(currentDate);
                                        foreignExchangenDetailService.insertStForeignExchangenDetail(detail);
                                    }
                                }
                            }
                        }
                    }else{
                        log.error("类型为"+ busiType + " 外汇信息不存在，请检查导入的数据文件是否正确.");
                    }
                }
            }
        }else {
            log.info("没有找到对应的外汇导入数据....");
        }
    }


    /**
     * 期货数据导入
     */
    public void importFutures(){
        log.info("开始导入期货数据信息.");
        Date currentDate = new Date();
        List<File> fileList=getFilesByName(IMPORT_TYPE_FUTURES);
        if(fileList !=null && fileList.size() > 0){
            for(File f : fileList){
                Sheet sheet = getSheet(f);
                if(sheet !=null){
                    String[]  fileNameSplit= f.getName().split("\\.")[0].split("_");
                    String busiType = fileNameSplit[1];    //分类
                    String busiDate = null;                //业务时间
                    Long futureType= 0L;                     //所属分类
                    String busiDateFormat = null;
                    String futureCode=null;  //代码
                    String futureName=null;  //名称
                    String nowPrice=null;    //现价
                    String buyPrice=null;    //买价
                    String sellPrice=null;   //卖价
                    String buyVolume=null;   //买量
                    String sellVolume=null;  //卖量
                    String highestPrice=null;  //最高价
                    String lowestPrice=null;   //最低价
                    String openPrice=null;     //开盘
                    String prevClose=null;     //昨收
                    String prevSettle=null;    //昨结
                    String todaySettle =null;  //今结
                    String upDownVal =null;   //涨跌
                    String upPrcent =null;    //涨幅%
                    String nowHands=null;      //现手
                    String totalHands =null;   //总手
                    String holdPosition =null; //持仓
                    String buyAmount  =null;   //外盘
                    String sellAmount =null;   //内盘
                    String amplitude   =null;   //振幅
                    String positions =null;     //日增仓
                    String totalAmount=null;   //总金额

                    Row row0 = sheet.getRow(0);
                    Row row = null;

                    for(int i=1;i<sheet.getLastRowNum();i++){
                        row = sheet.getRow(i);
                        if("大商所".equals(busiType)){
                            busiDate = fileNameSplit[2];
                            futureType = 4L;
                            if (row != null) {
                                futureCode   = getCellVal(row.getCell(1)) == null ? null : getCellVal(row.getCell(1)).toString();  //代码
                                futureName   = getCellVal(row.getCell(0)) == null ? null : getCellVal(row.getCell(0)).toString();  //名称
                                nowPrice     = getCellVal(row.getCell(3)) == null ? null : getCellVal(row.getCell(3)).toString();    //现价
                                upDownVal    = getCellVal(row.getCell(4)) == null ? null : getCellVal(row.getCell(4)).toString();   //涨跌
                                upPrcent     = getCellVal(row.getCell(5)) == null ? null : getCellVal(row.getCell(5)).toString();    //涨幅%
                                buyPrice     = getCellVal(row.getCell(6)) == null ? null : getCellVal(row.getCell(6)).toString();    //买价
                                sellPrice    =getCellVal(row.getCell(7)) == null ? null : getCellVal(row.getCell(7)).toString();   //卖价
                                buyVolume    =getCellVal(row.getCell(8)) == null ? null : getCellVal(row.getCell(8)).toString();   //买量
                                sellVolume   = getCellVal(row.getCell(9)) == null ? null : getCellVal(row.getCell(9)).toString();   //卖量
                                nowHands     =getCellVal(row.getCell(10)) == null ? null : getCellVal(row.getCell(10)).toString();  //现手
                                totalHands   =getCellVal(row.getCell(11)) == null ? null : getCellVal(row.getCell(11)).toString();   //总手
                                holdPosition =getCellVal(row.getCell(12)) == null ? null : getCellVal(row.getCell(12)).toString(); //持仓
                                positions    =getCellVal(row.getCell(13)) == null ? null : getCellVal(row.getCell(13)).toString();     //日增仓
                                amplitude    =getCellVal(row.getCell(14)) == null ? null : getCellVal(row.getCell(14)).toString();   //振幅
                                highestPrice = getCellVal(row.getCell(15)) == null ? null : getCellVal(row.getCell(15)).toString();  //最高价
                                lowestPrice  =getCellVal(row.getCell(16)) == null ? null : getCellVal(row.getCell(16)).toString();   //最低价
                                openPrice    =getCellVal(row.getCell(17)) == null ? null : getCellVal(row.getCell(17)).toString();     //开盘
                                prevClose    = getCellVal(row.getCell(18)) == null ? null : getCellVal(row.getCell(18)).toString();     //昨收
                                prevSettle   =getCellVal(row.getCell(19)) == null ? null : getCellVal(row.getCell(19)).toString();    //昨结
                                todaySettle  =getCellVal(row.getCell(20)) == null ? null : getCellVal(row.getCell(20)).toString();  //今结
                                buyAmount    =getCellVal(row.getCell(21)) == null ? null : getCellVal(row.getCell(21)).toString();   //外盘
                                sellAmount   =getCellVal(row.getCell(22)) == null ? null : getCellVal(row.getCell(22)).toString();   //内盘
                                totalAmount  =getCellVal(row.getCell(23)) == null ? null : getCellVal(row.getCell(23)).toString();   //总金额
                            }

                        }else if("境外交易所".equals(busiType)){
                            futureType = futuresMap.get(fileNameSplit[2]);
                            busiDate = fileNameSplit[3];

                            if (row != null) {
                                futureCode   =getCellVal(row.getCell(1)) == null ? null : getCellVal(row.getCell(1)).toString();  //代码
                                futureName   =getCellVal(row.getCell(0)) == null ? null : getCellVal(row.getCell(0)).toString();  //名称
                                buyPrice     = getCellVal(row.getCell(3)) == null ? null : getCellVal(row.getCell(3)).toString();    //买价
                                sellPrice    =getCellVal(row.getCell(4)) == null ? null : getCellVal(row.getCell(4)).toString();   //卖价
                                buyVolume    =getCellVal(row.getCell(5)) == null ? null : getCellVal(row.getCell(5)).toString();   //买量
                                sellVolume   =getCellVal(row.getCell(6)) == null ? null : getCellVal(row.getCell(6)).toString();  //卖量
                                nowPrice     =getCellVal(row.getCell(7)) == null ? null : getCellVal(row.getCell(7)).toString();    //现价
                                nowHands     =getCellVal(row.getCell(8)) == null ? null : getCellVal(row.getCell(8)).toString();      //现手
                                totalHands   =getCellVal(row.getCell(9)) == null ? null : getCellVal(row.getCell(9)).toString();   //总手
                                upDownVal    =getCellVal(row.getCell(10)) == null ? null : getCellVal(row.getCell(10)).toString();   //涨跌
                                upPrcent     =getCellVal(row.getCell(11)) == null ? null : getCellVal(row.getCell(11)).toString();    //涨幅%
                                holdPosition =getCellVal(row.getCell(14)) == null ? null : getCellVal(row.getCell(14)).toString(); //持仓
                                prevSettle   =getCellVal(row.getCell(15)) == null ? null : getCellVal(row.getCell(15)).toString();    //昨结
                                amplitude    =getCellVal(row.getCell(16)) == null ? null : getCellVal(row.getCell(16)).toString();   //振幅
                                prevClose    =getCellVal(row.getCell(17)) == null ? null : getCellVal(row.getCell(17)).toString();     //昨收
                                openPrice    =getCellVal(row.getCell(18)) == null ? null : getCellVal(row.getCell(18)).toString();     //开盘
                                highestPrice =getCellVal(row.getCell(19)) == null ? null : getCellVal(row.getCell(19)).toString();  //最高价
                                lowestPrice  =getCellVal(row.getCell(20)) == null ? null : getCellVal(row.getCell(20)).toString();   //最低价
                            }

                        }else if("上金所".equals(busiType)){
                            busiDate = fileNameSplit[2];
                            futureType = 6L;

                            if (row != null) {
                                futureCode   = getCellVal(row.getCell(1)) == null ? null : getCellVal(row.getCell(1)).toString();  //代码
                                futureName   = getCellVal(row.getCell(0)) == null ? null : getCellVal(row.getCell(0)).toString();  //名称
                                nowPrice     = getCellVal(row.getCell(3)) == null ? null : getCellVal(row.getCell(3)).toString();    //现价
                                nowHands     = getCellVal(row.getCell(4)) == null ? null : getCellVal(row.getCell(4)).toString();      //现手
                                upPrcent     = getCellVal(row.getCell(5)) == null ? null : getCellVal(row.getCell(5)).toString();    //涨幅%
                                upDownVal    = getCellVal(row.getCell(6)) == null ? null : getCellVal(row.getCell(6)).toString();   //涨跌
                                buyPrice     = getCellVal(row.getCell(7)) == null ? null : getCellVal(row.getCell(7)).toString();    //买价
                                sellPrice    = getCellVal(row.getCell(8)) == null ? null : getCellVal(row.getCell(8)).toString();   //卖价
                                buyVolume    = getCellVal(row.getCell(9)) == null ? null : getCellVal(row.getCell(9)).toString();   //买量
                                sellVolume   = getCellVal(row.getCell(10)) == null ? null : getCellVal(row.getCell(10)).toString();  //卖量
                                totalHands   = getCellVal(row.getCell(11)) == null ? null : getCellVal(row.getCell(11)).toString();   //总手
                                holdPosition = getCellVal(row.getCell(12)) == null ? null : getCellVal(row.getCell(12)).toString(); //持仓
                                prevSettle   = getCellVal(row.getCell(13)) == null ? null : getCellVal(row.getCell(13)).toString();    //昨结
                                todaySettle  = getCellVal(row.getCell(14)) == null ? null : getCellVal(row.getCell(14)).toString();  //今结
                                amplitude    = getCellVal(row.getCell(15)) == null ? null : getCellVal(row.getCell(15)).toString();   //振幅
                                prevClose    = getCellVal(row.getCell(16)) == null ? null : getCellVal(row.getCell(16)).toString();     //昨收
                                openPrice    = getCellVal(row.getCell(17)) == null ? null : getCellVal(row.getCell(17)).toString();     //开盘
                                highestPrice = getCellVal(row.getCell(18)) == null ? null : getCellVal(row.getCell(18)).toString();  //最高价
                                lowestPrice  = getCellVal(row.getCell(19)) == null ? null : getCellVal(row.getCell(19)).toString();   //最低价
                                totalAmount  = getCellVal(row.getCell(20)) == null ? null : getCellVal(row.getCell(20)).toString();   //总金额
                                buyAmount    = getCellVal(row.getCell(21)) == null ? null : getCellVal(row.getCell(21)).toString();   //外盘
                                sellAmount   = getCellVal(row.getCell(22)) == null ? null : getCellVal(row.getCell(22)).toString();   //内盘
                            }

                        }else if("上能源".equals(busiType)){
                            busiDate = fileNameSplit[2];
                            futureType = 7L;

                            if (row != null) {
                                futureCode    = getCellVal(row.getCell(1)) == null ? null : getCellVal(row.getCell(1)).toString();  //代码
                                futureName    = getCellVal(row.getCell(0)) == null ? null : getCellVal(row.getCell(0)).toString();  //名称
                                nowPrice      = getCellVal(row.getCell(3)) == null ? null : getCellVal(row.getCell(3)).toString();    //现价
                                upPrcent      = getCellVal(row.getCell(4)) == null ? null : getCellVal(row.getCell(4)).toString();    //涨幅%
                                upDownVal     = getCellVal(row.getCell(5)) == null ? null : getCellVal(row.getCell(5)).toString();   //涨跌
                                buyPrice      = getCellVal(row.getCell(6)) == null ? null : getCellVal(row.getCell(6)).toString();    //买价
                                sellPrice     = getCellVal(row.getCell(7)) == null ? null : getCellVal(row.getCell(7)).toString();   //卖价
                                buyVolume     = getCellVal(row.getCell(8)) == null ? null : getCellVal(row.getCell(8)).toString();   //买量
                                sellVolume    = getCellVal(row.getCell(9)) == null ? null : getCellVal(row.getCell(9)).toString();  //卖量
                                nowHands      = getCellVal(row.getCell(10)) == null ? null : getCellVal(row.getCell(10)).toString();      //现手
                                totalHands    = getCellVal(row.getCell(11)) == null ? null : getCellVal(row.getCell(11)).toString();   //总手
                                holdPosition  = getCellVal(row.getCell(12)) == null ? null : getCellVal(row.getCell(12)).toString(); //持仓
                                positions     = getCellVal(row.getCell(13)) == null ? null : getCellVal(row.getCell(13)).toString();     //日增仓
                                amplitude     = getCellVal(row.getCell(14)) == null ? null : getCellVal(row.getCell(14)).toString();   //振幅
                                highestPrice  = getCellVal(row.getCell(15)) == null ? null : getCellVal(row.getCell(15)).toString();  //最高价
                                lowestPrice   = getCellVal(row.getCell(16)) == null ? null : getCellVal(row.getCell(16)).toString();   //最低价
                                openPrice     = getCellVal(row.getCell(17)) == null ? null : getCellVal(row.getCell(17)).toString();     //开盘
                                prevClose     = getCellVal(row.getCell(18)) == null ? null : getCellVal(row.getCell(18)).toString();     //昨收
                                prevSettle    = getCellVal(row.getCell(19)) == null ? null : getCellVal(row.getCell(19)).toString();    //昨结
                                todaySettle   = getCellVal(row.getCell(20)) == null ? null : getCellVal(row.getCell(20)).toString();  //今结
                                buyAmount     = getCellVal(row.getCell(21)) == null ? null : getCellVal(row.getCell(21)).toString();   //外盘
                                sellAmount    = getCellVal(row.getCell(22)) == null ? null : getCellVal(row.getCell(22)).toString();   //内盘
                                totalAmount   = getCellVal(row.getCell(23)) == null ? null : getCellVal(row.getCell(23)).toString();   //总金额
                            }

                        }else if("上期所".equals(busiType)){
                            busiDate = fileNameSplit[2];
                            futureType = 3L;
                            if (row != null) {
                                futureCode    = getCellVal(row.getCell(1)) == null ? null : getCellVal(row.getCell(1)).toString();  //代码
                                futureName    = getCellVal(row.getCell(0)) == null ? null : getCellVal(row.getCell(0)).toString();  //名称
                                nowPrice      = getCellVal(row.getCell(3)) == null ? null : getCellVal(row.getCell(3)).toString();    //现价
                                upPrcent      = getCellVal(row.getCell(4)) == null ? null : getCellVal(row.getCell(4)).toString();    //涨幅%
                                upDownVal     = getCellVal(row.getCell(5)) == null ? null : getCellVal(row.getCell(5)).toString();   //涨跌
                                buyPrice      = getCellVal(row.getCell(6)) == null ? null : getCellVal(row.getCell(6)).toString();    //买价
                                sellPrice     = getCellVal(row.getCell(7)) == null ? null : getCellVal(row.getCell(7)).toString();   //卖价
                                buyVolume     = getCellVal(row.getCell(8)) == null ? null : getCellVal(row.getCell(8)).toString();   //买量
                                sellVolume    = getCellVal(row.getCell(9)) == null ? null : getCellVal(row.getCell(9)).toString();  //卖量
                                nowHands      = getCellVal(row.getCell(10)) == null ? null : getCellVal(row.getCell(10)).toString();      //现手
                                totalHands    = getCellVal(row.getCell(11)) == null ? null : getCellVal(row.getCell(11)).toString();   //总手
                                holdPosition  = getCellVal(row.getCell(12)) == null ? null : getCellVal(row.getCell(12)).toString(); //持仓
                                positions     = getCellVal(row.getCell(13)) == null ? null : getCellVal(row.getCell(13)).toString();     //日增仓
                                amplitude     = getCellVal(row.getCell(14)) == null ? null : getCellVal(row.getCell(14)).toString();   //振幅
                                highestPrice  = getCellVal(row.getCell(15)) == null ? null : getCellVal(row.getCell(15)).toString();  //最高价
                                lowestPrice   = getCellVal(row.getCell(16)) == null ? null : getCellVal(row.getCell(16)).toString();   //最低价
                                openPrice     = getCellVal(row.getCell(17)) == null ? null : getCellVal(row.getCell(17)).toString();     //开盘
                                prevClose     = getCellVal(row.getCell(18)) == null ? null : getCellVal(row.getCell(18)).toString();     //昨收
                                prevSettle    = getCellVal(row.getCell(19)) == null ? null : getCellVal(row.getCell(19)).toString();    //昨结
                                todaySettle   = getCellVal(row.getCell(20)) == null ? null : getCellVal(row.getCell(20)).toString();  //今结
                                buyAmount     = getCellVal(row.getCell(21)) == null ? null : getCellVal(row.getCell(21)).toString();   //外盘
                                sellAmount    = getCellVal(row.getCell(22)) == null ? null : getCellVal(row.getCell(22)).toString();   //内盘
                                totalAmount   = getCellVal(row.getCell(23)) == null ? null : getCellVal(row.getCell(23)).toString();   //总金额
                            }

                        }else if("郑商所".equals(busiType)){
                            busiDate = fileNameSplit[2];
                            futureType = 5L;

                            if (row != null) {
                                futureCode    = getCellVal(row.getCell(1)) == null ? null : getCellVal(row.getCell(1)).toString();  //代码
                                futureName    = getCellVal(row.getCell(0)) == null ? null : getCellVal(row.getCell(0)).toString();  //名称
                                nowPrice      = getCellVal(row.getCell(3)) == null ? null : getCellVal(row.getCell(3)).toString();    //现价
                                upPrcent      = getCellVal(row.getCell(4)) == null ? null : getCellVal(row.getCell(4)).toString();    //涨幅%
                                upDownVal     = getCellVal(row.getCell(5)) == null ? null : getCellVal(row.getCell(5)).toString();   //涨跌
                                buyPrice      = getCellVal(row.getCell(6)) == null ? null : getCellVal(row.getCell(6)).toString();    //买价
                                sellPrice     = getCellVal(row.getCell(7)) == null ? null : getCellVal(row.getCell(7)).toString();   //卖价
                                buyVolume     = getCellVal(row.getCell(8)) == null ? null : getCellVal(row.getCell(8)).toString();   //买量
                                sellVolume    = getCellVal(row.getCell(9)) == null ? null : getCellVal(row.getCell(9)).toString();  //卖量
                                nowHands      = getCellVal(row.getCell(10)) == null ? null : getCellVal(row.getCell(10)).toString();      //现手
                                totalHands    = getCellVal(row.getCell(11)) == null ? null : getCellVal(row.getCell(11)).toString();   //总手
                                holdPosition  = getCellVal(row.getCell(12)) == null ? null : getCellVal(row.getCell(12)).toString(); //持仓
                                positions     = getCellVal(row.getCell(13)) == null ? null : getCellVal(row.getCell(13)).toString();     //日增仓
                                amplitude     = getCellVal(row.getCell(14)) == null ? null : getCellVal(row.getCell(14)).toString();   //振幅
                                highestPrice  = getCellVal(row.getCell(15)) == null ? null : getCellVal(row.getCell(15)).toString();  //最高价
                                lowestPrice   = getCellVal(row.getCell(16)) == null ? null : getCellVal(row.getCell(16)).toString();   //最低价
                                openPrice     = getCellVal(row.getCell(17)) == null ? null : getCellVal(row.getCell(17)).toString();     //开盘
                                prevClose     = getCellVal(row.getCell(18)) == null ? null : getCellVal(row.getCell(18)).toString();     //昨收
                                prevSettle    = getCellVal(row.getCell(19)) == null ? null : getCellVal(row.getCell(19)).toString();    //昨结
                                todaySettle   = getCellVal(row.getCell(20)) == null ? null : getCellVal(row.getCell(20)).toString();  //今结
                                buyAmount     = getCellVal(row.getCell(21)) == null ? null : getCellVal(row.getCell(21)).toString();   //外盘
                                sellAmount    = getCellVal(row.getCell(22)) == null ? null : getCellVal(row.getCell(22)).toString();   //内盘
                                totalAmount   = getCellVal(row.getCell(23)) == null ? null : getCellVal(row.getCell(23)).toString();   //总金额

                            }

                        }else if("中金所".equals(busiType)){
                            futureType = futuresMap.get(fileNameSplit[2]);
                            busiDate = fileNameSplit[3];

                            if (row != null) {
                                futureCode    = getCellVal(row.getCell(0)) == null ? null : getCellVal(row.getCell(0)).toString();  //代码
                                futureName    = getCellVal(row.getCell(1)) == null ? null : getCellVal(row.getCell(1)).toString();  //名称
                                nowPrice      = getCellVal(row.getCell(3)) == null ? null : getCellVal(row.getCell(3)).toString();    //现价
                                upPrcent      = getCellVal(row.getCell(4)) == null ? null : getCellVal(row.getCell(4)).toString();    //涨幅%
                                upDownVal     = getCellVal(row.getCell(5)) == null ? null : getCellVal(row.getCell(5)).toString();   //涨跌
                                buyPrice      = getCellVal(row.getCell(7)) == null ? null : getCellVal(row.getCell(7)).toString();    //买价
                                sellPrice     = getCellVal(row.getCell(8)) == null ? null : getCellVal(row.getCell(8)).toString();   //卖价
                                buyVolume     = getCellVal(row.getCell(9)) == null ? null : getCellVal(row.getCell(9)).toString();   //买量
                                sellVolume    = getCellVal(row.getCell(10)) == null ? null : getCellVal(row.getCell(10)).toString();  //卖量
                                nowHands      = getCellVal(row.getCell(11)) == null ? null : getCellVal(row.getCell(11)).toString();      //现手
                                totalHands    = getCellVal(row.getCell(12)) == null ? null : getCellVal(row.getCell(12)).toString();   //总手
                                holdPosition  = getCellVal(row.getCell(13)) == null ? null : getCellVal(row.getCell(13)).toString(); //持仓
                                positions     = getCellVal(row.getCell(14)) == null ? null : getCellVal(row.getCell(14)).toString();     //日增仓
                                totalAmount   = getCellVal(row.getCell(15)) == null ? null : getCellVal(row.getCell(15)).toString();   //总金额
                                prevSettle    = getCellVal(row.getCell(16)) == null ? null : getCellVal(row.getCell(16)).toString();    //昨结
                                todaySettle   = getCellVal(row.getCell(17)) == null ? null : getCellVal(row.getCell(17)).toString();  //今结
                                amplitude     = getCellVal(row.getCell(20)) == null ? null : getCellVal(row.getCell(20)).toString();   //振幅
                                prevClose     = getCellVal(row.getCell(22)) == null ? null : getCellVal(row.getCell(22)).toString();     //昨收
                                openPrice     = getCellVal(row.getCell(23)) == null ? null : getCellVal(row.getCell(23)).toString();     //开盘
                                highestPrice  = getCellVal(row.getCell(24)) == null ? null : getCellVal(row.getCell(24)).toString();  //最高价
                                lowestPrice   = getCellVal(row.getCell(25)) == null ? null : getCellVal(row.getCell(25)).toString();   //最低价
                                buyAmount     = getCellVal(row.getCell(26)) == null ? null : getCellVal(row.getCell(26)).toString();   //外盘
                                sellAmount    = getCellVal(row.getCell(27)) == null ? null : getCellVal(row.getCell(27)).toString();   //内盘

                            }

                        }
                        //处理每一条记录
                        if(futureCode !=null && !"".equals(futureCode)){
                            //判断当前的编码是否存在
                            StFutures futures =futuresService.selectStFuturesByCode(futureCode);
                            busiDateFormat = busiDate.substring(0,4) + "-" + busiDate.substring(4,6) + "-" + busiDate.substring(6,8);
                            if(futures==null){
                                //新增
                                futures = new StFutures();
                                futures.setFutureName(futureName);
                                futures.setFutureCode(futureCode);
                                futures.setFutureType(futureType.toString());
                                futures.setCreateTime(currentDate);
                                futures.setCreateBy("Admin");
                                int res= futuresService.insertStFutures(futures);
                            }else{
                                log.info("代码" +futureCode +"的期货数据已存在，无需重复插入.");
                            }
                            if(futures !=null){
                                //判断futures_detail表是否存在
                                StFuturesDetail detail = new StFuturesDetail();
                                detail.setFutureId(futures.getId());
                                detail.setDataTime(DateUtils.parseDate(busiDateFormat));
                                List<StFuturesDetail> list=futuresDetailService.selectStFuturesDetailList(detail);
                                if(list ==null || list.size()==0){
                                    //新增
                                    if(NumberUtils.isNum(nowPrice)){
                                        detail.setNowPrice(Double.parseDouble(nowPrice));      //现价
                                    }
                                    if(NumberUtils.isNum(buyPrice)){
                                        detail.setBuyPrice(Double.parseDouble(buyPrice));       //买价
                                    }
                                    if(NumberUtils.isNum(sellPrice)){
                                        detail.setSellPrice(Double.parseDouble(sellPrice));      //卖价
                                    }
                                    if(NumberUtils.isNum(buyVolume)){
                                        detail.setBuyVolume((new Double(buyVolume)).longValue());         //买量
                                    }
                                    if(NumberUtils.isNum(sellVolume)){
                                        detail.setSellVolume((new Double(sellVolume)).longValue());         //卖量
                                    }
                                    if(NumberUtils.isNum(highestPrice)){
                                        detail.setHighestPrice(Double.parseDouble(highestPrice));         //最高价
                                    }
                                    if(NumberUtils.isNum(lowestPrice)){
                                        detail.setLowestPrice(Double.parseDouble(lowestPrice));         //最低价
                                    }
                                  /*  if(NumberUtils.isNum(sellVolume)){
                                        detail.setSellVolume(Long.parseLong(sellVolume));         //卖量
                                    }*/
                                    if(NumberUtils.isNum(openPrice)){
                                        detail.setOpenPrice(Double.parseDouble(openPrice));         //开盘
                                    }
                                    if(NumberUtils.isNum(prevClose)){
                                        detail.setPrevClose(Double.parseDouble(prevClose));         //昨收
                                    }
                                    if(NumberUtils.isNum(prevSettle)){
                                        detail.setPrevSettle(Double.parseDouble(prevSettle));         //昨结
                                    }
                                    if(NumberUtils.isNum(todaySettle)){
                                        detail.setTodaySettle(Double.parseDouble(todaySettle));         //今结
                                    }
                                    if(NumberUtils.isNum(totalHands)){
                                        detail.setTotalHands((new Double(totalHands)).longValue());         //总手
                                    }

                                    if(NumberUtils.isNum(buyAmount)){
                                        detail.setBuyAmount((new Double(buyAmount)).longValue());         //外盘
                                    }
                                    if(NumberUtils.isNum(sellAmount)){
                                        detail.setSellAmount((new Double(sellAmount)).longValue());         //内盘
                                    }
                                    if(NumberUtils.isNum(holdPosition)){
                                        detail.setHoldPosition((new Double(holdPosition)).longValue());         //持仓
                                    }
                                    if(NumberUtils.isNum(positions)){
                                        detail.setPositions((new Double(positions)).longValue());         //日增仓
                                    }

                                    //nowHands      = getCellVal(row.getCell(0)) == null ? null : getCellVal(row.getCell(0)).toString();      //现手
                                    if(amplitude !=null && amplitude.contains("%")){                                       //振幅
                                        amplitude = amplitude.substring(0,amplitude.length() -1);
                                        detail.setAmplitude(Double.parseDouble(amplitude) / 100);
                                    }else{
                                        if(NumberUtils.isNum(amplitude)){
                                            detail.setAmplitude(Double.parseDouble(amplitude));
                                        }
                                    }

                                    if(NumberUtils.isNum(upDownVal)){
                                        detail.setUpDownVal(Double.parseDouble(upDownVal));         //涨跌
                                    }
                                    if(NumberUtils.isNum(upPrcent)){
                                        detail.setUpPercent(Double.parseDouble(upPrcent));         //涨幅%
                                    }

                                    if(NumberUtils.isNum(totalAmount)){
                                        detail.setTotalAmount((new Double(totalAmount)).longValue());         //总金额
                                    }
                                    futuresDetailService.insertStFuturesDetail(detail);
                                }
                            }
                        }
                    }
                }
            }
        }else {
            log.info("没有找到对应的外汇导入数据....");
        }
    }



    /**
     * 根据文件返回对应的sheet对象，默认第一个sheet页
     * @param file
     * @return
     */
    public Sheet getSheet(File file){
        Workbook wk = null;
        try {
            String fileName = file.getName();
            String suffix= fileName.substring(fileName.lastIndexOf(".")+1,fileName.length());
            if(suffix.toLowerCase().equals("xls")){
                wk = new HSSFWorkbook(new FileInputStream(file));
            }else if(suffix.toLowerCase().equals("xlsx")){
                wk = new XSSFWorkbook(new FileInputStream(file));
            }
            if(wk !=null){
                return  wk.getSheetAt(0);
            }
        } catch (Exception e) {
            log.error("根据文件，获取导入的文件的Sheet页对象出错......");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 根据文件名称查找文件
     * @param fileName
     * @return
     */
    public List<File> getFilesByName(String fileNameRegExp){
        List<File> list =null;
        try {
            File file  = new File(DATA_FILE_PATH);
            if (file !=null){
                File[] fileArray = file.listFiles();
                for (File f:fileArray){
                    if(f.getName().startsWith(fileNameRegExp)){
                        if(list ==null){
                            list = new ArrayList<File>();
                        }
                        list.add(f);
                    }
                }
            }
        } catch (Exception e) {
            log.error("根据文件名，获取导入的数据文件出错.....");
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 获取单元格的值
     * @param args
     */
    public Object getCellVal(Cell cell){
        if(cell !=null){
            switch (cell.getCellType()){
                case Cell.CELL_TYPE_NUMERIC:  //数字
                    return  cell.getNumericCellValue();
                case  Cell.CELL_TYPE_BLANK:   //空格
                    return null;
                case Cell.CELL_TYPE_STRING:  //字符串
                    return cell.getRichStringCellValue().getString();
                case Cell.CELL_TYPE_FORMULA:  //公式
                    return cell.getCellFormula() + "";
                case Cell.CELL_TYPE_ERROR:    //错误
                    return null;
                    default:
                        return  null;
            }

        }
        return null;
    }

    public static void main(String[] args) {

    }
}
