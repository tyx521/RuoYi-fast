package com.ruoyi.common.utils;

/**
 * 数字处理工具
 * @date: 2019-08-13
 * @author : tingyu
 */
public class NumberUtils {
    /**
     * 判断是否是数字(正数，负数，小数)
     * @param str
     * @return
     */
    public static  boolean isNum(String str){
        if(str ==null || "".equals(str)){
            return false;
        }
        return str.matches("-?[0-9]+.*[0-9]*");
    }

    public static void main(String[] args){
        System.out.println(NumberUtils.isNum("-12.34"));
    }
}
