import com.ruoyi.RuoYiApplication;
import com.ruoyi.project.monitor.job.task.DataBatchImportTask;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RuoYiApplication.class)
@WebAppConfiguration
public class TestDataBatchImportTask {
    @Autowired
    private DataBatchImportTask task;

    //债券数据导入
    @Test
    public void testImportBond(){
        task.importBond();
    }

    //基金数据导入
    @Test
    public void testDownlaodFoundationDataByNet(){
        task.downlaodFoundationDataByNet();
    }

    //导入外汇数据信息
    @Test
    public void testImportExchange(){
        task.importExchange();
    }

    //导入期货数据
    @Test
    public void testImportFutures(){
        task.importFutures();
    }

}
